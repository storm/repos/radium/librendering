cmake_minimum_required(VERSION 3.6)
#------------------------------------------------------------------------------
# Policies and global parameters for CMake
if (POLICY CMP0077)
    cmake_policy(SET CMP0077 NEW)
endif ()
if (APPLE)
    cmake_policy(SET CMP0042 NEW)
endif (APPLE)

set(CMAKE_DISABLE_SOURCE_CHANGES ON)
set(CMAKE_DISABLE_IN_SOURCE_BUILD ON)

project(NodeRendererPlugin VERSION 1.0.0)

if (CMAKE_INSTALL_PREFIX_INITIALIZED_TO_DEFAULT)
    set(CMAKE_INSTALL_PREFIX "${CMAKE_CURRENT_BINARY_DIR}/installed-${CMAKE_CXX_COMPILER_ID}" CACHE PATH
        "Install path prefix, prepended onto install directories." FORCE)
    message("Set install prefix to ${CMAKE_INSTALL_PREFIX}")
    set(CMAKE_INSTALL_PREFIX_INITIALIZED_TO_DEFAULT False)
endif ()

# Radium and Qt stuff
find_package(Radium REQUIRED Core Engine PluginBase)
find_package(Qt5 COMPONENTS Core Widgets REQUIRED)
set(Qt5_LIBRARIES Qt5::Core Qt5::Widgets)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)
include_directories(
    ${CMAKE_CURRENT_BINARY_DIR} # Moc
)

set(CMAKE_INCLUDE_CURRENT_DIR ON)

set(sources
    NodeRendererPlugin.cpp
    )

set(headers
    NodeRendererPlugin.hpp
    NodeRendererPluginMacros.hpp
    )

# Our library project uses these sources and headers.
add_library(
    ${PROJECT_NAME} SHARED
    ${sources}
    ${headers}
)


# Include look up directories
target_include_directories(${PROJECT_NAME} PRIVATE
    ../libRender
    )
target_compile_definitions(${PROJECT_NAME} PRIVATE ${PROJECT_NAME}_EXPORTS)

target_link_libraries(
    ${PROJECT_NAME}
    Radium::Core
    Radium::Engine
    Radium::PluginBase
    ${Qt5_LIBRARIES}
    RadiumNBR::NBRGui
)

message("Configure plugin ${PROJECT_NAME} for insertion into Radium exosystem")

configure_radium_plugin(
    NAME ${PROJECT_NAME}
    HELPER_LIBS RadiumNBR::NBR RadiumNBR::NBRGui
    #        INSTALL_IN_RADIUM_BUNDLE
)

