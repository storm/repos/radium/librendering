#pragma once

#include <Core/CoreMacros.hpp>
#include <NodeRendererPluginMacros.hpp>

#include <PluginBase/RadiumPluginInterface.hpp>

#include <RadiumNBR/Renderer/FullFeatureRenderer.hpp>

namespace RadiumNBRPlugin {

/*!
 * \brief This plugin gives access to a node based renderer (still under development) under Radium.
 *
 */
class NodeRenderer_PLUGIN_API NodeRendererPlugin : public QObject,
                                                   Ra::Plugins::RadiumPluginInterface
{
    Q_OBJECT
    Q_RADIUM_PLUGIN_METADATA
    Q_INTERFACES( Ra::Plugins::RadiumPluginInterface )

  public:
    NodeRendererPlugin() = default;

    ~NodeRendererPlugin() override = default;

    void registerPlugin( const Ra::Plugins::Context& context ) override;

    bool doAddWidget( QString& name ) override;
    QWidget* getWidget() override;
    bool doAddMenu() override;
    QMenu* getMenu() override;
    bool doAddAction( int& nb ) override;
    QAction* getAction( int id ) override;

    bool doAddRenderer() override;
    void
    addRenderers( std::vector<std::shared_ptr<Ra::Engine::Rendering::Renderer>>* rds ) override;
    bool doAddROpenGLInitializer() override;

    //    void openGlInitialize( const Ra::Plugins::Context& context ) override;

  signals:
    void askForUpdate();

  private:
    std::shared_ptr<RadiumNBR::FullFeatureRenderer> m_renderer;
};

} // namespace RadiumNBRPlugin
