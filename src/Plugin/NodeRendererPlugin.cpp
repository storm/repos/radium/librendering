#include <NodeRendererPlugin.hpp>

#include <RadiumNBR/Gui/FullFeaturedRendererGui.hpp>

namespace RadiumNBRPlugin {

void NodeRendererPlugin::registerPlugin( const Ra::Plugins::Context& context ) {
    connect(
        this, &NodeRendererPlugin::askForUpdate, &context, &Ra::Plugins::Context::askForUpdate );
}

bool NodeRendererPlugin::doAddWidget( QString& name ) {
    name = "NodeRendererPlugin";
    return true;
}

QWidget* NodeRendererPlugin::getWidget() {
    if ( !m_renderer ) { m_renderer = std::make_shared<RadiumNBR::FullFeatureRenderer>(); }
    auto sendSignal = [this]() { emit askForUpdate(); };
    return RadiumNBR::buildRadiumNBRGui( m_renderer.get(), sendSignal );
}

bool NodeRendererPlugin::doAddMenu() {
    return false;
}

QMenu* NodeRendererPlugin::getMenu() {
    return nullptr;
}

bool NodeRendererPlugin::doAddAction( int& /*nb*/ ) {
    return false;
}

QAction* NodeRendererPlugin::getAction( int /*id*/ ) {
    return nullptr;
}

bool NodeRendererPlugin::doAddROpenGLInitializer() {
    return false;
}
bool NodeRendererPlugin::doAddRenderer() {
    return true;
}
void NodeRendererPlugin::addRenderers(
    std::vector<std::shared_ptr<Ra::Engine::Rendering::Renderer>>* rds ) {
    if ( !m_renderer ) { m_renderer = std::make_shared<RadiumNBR::FullFeatureRenderer>(); }
    rds->push_back( m_renderer );
}

} // namespace RadiumNBRPlugin
