cmake_minimum_required(VERSION 3.6)
#------------------------------------------------------------------------------
# Policies and global parameters for CMake
if (POLICY CMP0077)
    # allow to define options cache variable before the option is declared
    # https://cmake.org/cmake/help/latest/policy/CMP0077.html
    cmake_policy(SET CMP0077 NEW)
endif ()
if (APPLE)
    # MACOSX_RPATH is enabled by default.
    # https://cmake.org/cmake/help/latest/policy/CMP0042.html
    cmake_policy(SET CMP0042 NEW)
endif (APPLE)
set(CMAKE_DISABLE_SOURCE_CHANGES ON)
set(CMAKE_DISABLE_IN_SOURCE_BUILD ON)

project(RadiumH3D VERSION 1.0.0)
set(libName H3D)

if (CMAKE_INSTALL_PREFIX_INITIALIZED_TO_DEFAULT)
    set(CMAKE_INSTALL_PREFIX "${CMAKE_CURRENT_BINARY_DIR}/installed-${CMAKE_CXX_COMPILER_ID}" CACHE PATH
        "Install path prefix, prepended onto install directories." FORCE)
    message("Set install prefix to ${CMAKE_INSTALL_PREFIX}")
    set(CMAKE_INSTALL_PREFIX_INITIALIZED_TO_DEFAULT False)
endif ()

# Radium stuff
find_package(Radium REQUIRED Core Engine)

set(markdowns
    #        Readme.md
    )

set(sources
    RadiumH3D/h3DLibrary.cpp
    RadiumH3D/h3DLoader.cpp
    RadiumH3D/Model/h3DModel.cpp
    )

set(public_headers
    RadiumH3D/h3DLibrary.hpp
    RadiumH3D/h3DLoader.hpp
    RadiumH3D/h3DMacros.hpp
    )

set(headers
    RadiumH3D/Model/h3DModel.hpp
    )

set(resources
    )


# Our library project uses these sources and headers.
add_library(
    ${libName} SHARED
    ${sources}
    ${headers}
    ${public_headers}
    ${markdowns}
    ${resources}
)

target_compile_definitions(${libName} PRIVATE LIBRARY_NAME=\"${PROJECT_NAME}\")

target_link_libraries(
    ${libName}
    PUBLIC
    Radium::Core
    Radium::Engine
)


#-----------------------------------------------------------------------------------

message("Configure library ${PROJECT_NAME} for insertion into Radium exosystem")
configure_radium_library(
    TARGET ${libName}
    TARGET_DIR ${PROJECT_NAME}
    NAMESPACE ${PROJECT_NAME}
    PACKAGE_DIR ${CMAKE_INSTALL_PREFIX}/lib/cmake
    PACKAGE_CONFIG ${CMAKE_CURRENT_SOURCE_DIR}/Config.cmake.in
    FILES "${public_headers}"
)

# no resources for now
#installTargetResources(
#        TARGET ${PROJECT_NAME}
#        PREFIX ${PROJECT_NAME}
#        DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/Resources/Shaders
#)

