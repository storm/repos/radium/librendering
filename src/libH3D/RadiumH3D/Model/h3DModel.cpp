//
// Created by Mathias Paulin on 22/09/2020.
//
#include <RadiumH3D/Model/h3DModel.hpp>

#include <Core/Asset/BlinnPhongMaterialData.hpp>
#include <Core/Asset/FileData.hpp>
#include <Core/Asset/GeometryData.hpp>
#include <Core/Utils/Log.hpp>

#include <Core/Containers/VectorArray.hpp>

namespace H3D {
using namespace Ra::Core;
using namespace Ra::Core::Utils;
using namespace Ra::Core::Asset;

H3DModel::H3DModel( Ra::Core::Asset::FileData* fd, const std::string& baseDir ) :
    m_fileData{ fd }, m_filePath{ baseDir } {}

// used to convert 3 components attribs
template <typename T>
void convertVec3( Vector3Array& vectors, const unsigned char* data, int count, int stride ) {

    for ( int i = 0; i < count; ++i )
    {
        auto mem = reinterpret_cast<const T*>( data + ( i * stride ) );
        vectors.emplace_back( mem[0], mem[1], mem[2] );
    }
}

// used to convert 2 components attribs (texCoord)
template <typename T>
void convertVec2( Vector3Array& vectors, const unsigned char* data, int count, int stride ) {

    for ( int i = 0; i < count; ++i )
    {
        auto mem = reinterpret_cast<const T*>( data + ( i * stride ) );
        vectors.emplace_back( mem[0], mem[1], 0 );
    }
}

// used to convert face indices
template <typename T>
void convertIndices( VectorNuArray& indices, const uint8_t* data, uint32_t count ) {
    auto mem = reinterpret_cast<const T*>( data );
    for ( int i = 0; i < count; ++i )
    {
        indices.push_back( Vector3ui{ mem[3 * i], mem[3 * i + 1], mem[3 * i + 2] } );
    }
}

bool H3DModel::operator()( const std::string& filename ) {
    FILE* file = nullptr;

#if 0
    if (0 != fopen_s(&file, filename, "rb"))
        return false;
#else
    file = fopen( filename.c_str(), "rb" );
    if ( nullptr == file ) return false;
#endif

    bool ok = false;

    if ( 1 != fread( &m_Header, sizeof( Header ), 1, file ) ) goto h3d_load_fail;

    m_pMesh     = new Mesh[m_Header.meshCount];
    m_pMaterial = new Material[m_Header.materialCount];

    if ( m_Header.meshCount > 0 )
        if ( 1 != fread( m_pMesh, sizeof( Mesh ) * m_Header.meshCount, 1, file ) )
            goto h3d_load_fail;
    if ( m_Header.materialCount > 0 )
        if ( 1 != fread( m_pMaterial, sizeof( Material ) * m_Header.materialCount, 1, file ) )
            goto h3d_load_fail;

    m_VertexStride      = m_pMesh[0].vertexStride;
    m_VertexStrideDepth = m_pMesh[0].vertexStrideDepth;

    LOG( logINFO ) << "Loaded " << filename << " :";
    LOG( logINFO ) << "\tmeshCount\t: " << m_Header.meshCount;
    LOG( logINFO ) << "\tmaterialCount\t: " << m_Header.materialCount;
    LOG( logINFO ) << "\tvertexDataByteSize\t: " << m_Header.vertexDataByteSize;
    LOG( logINFO ) << "\tindexDataByteSize\t: " << m_Header.indexDataByteSize;
    LOG( logINFO ) << "\tvertexDataByteSizeDepth\t: " << m_Header.vertexDataByteSizeDepth;
    LOG( logINFO ) << "\tboundingBox\t: "
                   << "(" << m_Header.boundingBox.min.v[0] << ", " << m_Header.boundingBox.min.v[1]
                   << ", " << m_Header.boundingBox.min.v[2] << ") -- ("
                   << m_Header.boundingBox.max.v[0] << ", " << m_Header.boundingBox.max.v[1] << ", "
                   << m_Header.boundingBox.max.v[2] << ")";
    LOG( logINFO ) << "\tm_VertexStride\t: " << m_VertexStride;

    m_pVertexData      = new unsigned char[m_Header.vertexDataByteSize];
    m_pIndexData       = new unsigned char[m_Header.indexDataByteSize];
    m_pVertexDataDepth = new unsigned char[m_Header.vertexDataByteSizeDepth];
    m_pIndexDataDepth  = new unsigned char[m_Header.indexDataByteSize];

    if ( m_Header.vertexDataByteSize > 0 )
        if ( 1 != fread( m_pVertexData, m_Header.vertexDataByteSize, 1, file ) ) goto h3d_load_fail;
    if ( m_Header.indexDataByteSize > 0 )
        if ( 1 != fread( m_pIndexData, m_Header.indexDataByteSize, 1, file ) ) goto h3d_load_fail;

    if ( m_Header.vertexDataByteSizeDepth > 0 )
        if ( 1 != fread( m_pVertexDataDepth, m_Header.vertexDataByteSizeDepth, 1, file ) )
            goto h3d_load_fail;
    if ( m_Header.indexDataByteSize > 0 )
        if ( 1 != fread( m_pIndexDataDepth, m_Header.indexDataByteSize, 1, file ) )
            goto h3d_load_fail;

    for ( uint32_t meshIndex = 0; meshIndex < m_Header.meshCount; ++meshIndex )
    {
        const Mesh& mesh = m_pMesh[meshIndex];
        std::string meshName{ "mesh_" };
        meshName += std::to_string( meshIndex );
#if 0
        LOG( logINFO ) << "Mesh " << meshIndex << " : " << meshName;
        LOG( logINFO ) << "\tVertices " << mesh.vertexCount << " starting at " << mesh.vertexDataByteOffset;
        LOG( logINFO ) << "\tIndices " << mesh.indexCount << " -- (" << mesh.indexCount/3 << ")" << " starting at " << mesh.indexDataByteOffset;
        LOG( logINFO ) << "\tStrides : vertex " << mesh.vertexStride << " -- indices " << mesh.indexDataByteOffset;
        LOG( logINFO ) << "\tAttributes " << mesh.attribsEnabled;
        for(uint32_t attribIndex = 0; attribIndex < maxAttribs; ++attribIndex ) {
            if ( mesh.attribsEnabled & (1 << attribIndex) ) {
                LOG( logINFO ) << "\t\tAttrib " << attribIndex
                               << " -- " << mesh.attrib[attribIndex].offset
                               << " -- " << mesh.attrib[attribIndex].normalized
                               << " -- " << mesh.attrib[attribIndex].components
                               << " -- " << mesh.attrib[attribIndex].format;
            }
        }
        LOG( logINFO ) << "\tMaterial " << mesh.materialIndex;
#endif

        auto meshPart =
            std::make_unique<GeometryData>( meshName, GeometryData::GeometryType::TRI_MESH );
        // store vertices
        if ( mesh.attribsEnabled & ( 1 << attrib_position ) )
        {
            convertVec3<float>( meshPart->getVertices(),
                                m_pVertexData + mesh.vertexDataByteOffset +
                                    mesh.attrib[attrib_position].offset,
                                mesh.vertexCount,
                                mesh.vertexStride );
        }
        else
        { LOG( logERROR ) << "Mesh must have vertices position !"; }
        // store normals
        if ( mesh.attribsEnabled & ( 1 << attrib_normal ) )
        {
            convertVec3<float>( meshPart->getNormals(),
                                m_pVertexData + mesh.vertexDataByteOffset +
                                    mesh.attrib[attrib_normal].offset,
                                mesh.vertexCount,
                                mesh.vertexStride );
        }
        // store texCoord
        if ( mesh.attribsEnabled & ( 1 << attrib_texcoord0 ) )
        {
            convertVec2<float>( meshPart->getTexCoords(),
                                m_pVertexData + mesh.vertexDataByteOffset +
                                    mesh.attrib[attrib_texcoord0].offset,
                                mesh.vertexCount,
                                mesh.vertexStride );
        }
        // store tangents
        if ( mesh.attribsEnabled & ( 1 << attrib_tangent ) )
        {
            convertVec3<float>( meshPart->getNormals(),
                                m_pVertexData + mesh.vertexDataByteOffset +
                                    mesh.attrib[attrib_tangent].offset,
                                mesh.vertexCount,
                                mesh.vertexStride );
        }
        // store indices
        convertIndices<uint16_t>(
            meshPart->getFaces(), m_pIndexData + mesh.indexDataByteOffset, mesh.indexCount / 3 );

        // material
        const std::string textureExt( ".png" );

        const auto& material = m_pMaterial[mesh.materialIndex];
        LOG( logINFO ) << "Material : " << material.name;
        LOG( logINFO ) << "\tspecular : " << material.specular.v[0] << " " << material.specular.v[1]
                       << " " << material.specular.v[2];
        LOG( logINFO ) << "\tspecStrength : " << material.specularStrength;
        LOG( logINFO ) << "\tshininess : " << material.shininess;

        LOG( logINFO ) << "Building material " << material.name;
        auto blinnPhongMaterial = new BlinnPhongMaterialData( material.name );
        // diffuse
        blinnPhongMaterial->m_hasDiffuse = true;
        blinnPhongMaterial->m_diffuse    = Ra::Core::Utils::Color(
            material.diffuse.v[0], material.diffuse.v[1], material.diffuse.v[2], 1_ra );
        if ( material.texDiffusePath[0] != '\0' )
        {
            LOG( logINFO ) << "\tFound diffuse texture " << material.texDiffusePath;
            blinnPhongMaterial->m_texDiffuse    = m_filePath + material.texDiffusePath + textureExt;
            blinnPhongMaterial->m_hasTexDiffuse = true;
        }
        else
        { blinnPhongMaterial->m_hasTexDiffuse = false; }
        // specular
        blinnPhongMaterial->m_hasSpecular = true;
        blinnPhongMaterial->m_specular =
            Ra::Core::Utils::Color( material.specular.v[0] * material.specularStrength,
                                    material.specular.v[1] * material.specularStrength,
                                    material.specular.v[2] * material.specularStrength,
                                    1_ra );
        if ( material.texSpecularPath[0] != '\0' )
        {
            LOG( logINFO ) << "\tFound specular texture " << material.texSpecularPath;
            blinnPhongMaterial->m_texSpecular = m_filePath + material.texSpecularPath + textureExt;
            blinnPhongMaterial->m_hasTexSpecular = true;
        }
        else
        { blinnPhongMaterial->m_hasTexSpecular = false; }

        // no ambiant
        // no emmissive
        // shininess and opacity
        blinnPhongMaterial->m_hasShininess = true;
        blinnPhongMaterial->m_shininess    = material.shininess;
        blinnPhongMaterial->m_hasOpacity   = true;
        blinnPhongMaterial->m_opacity      = material.opacity;
        // normal map
        if ( material.texNormalPath[0] != '\0' )
        {
            LOG( logINFO ) << "\tFound normal texture " << material.texNormalPath;
            blinnPhongMaterial->m_texNormal    = m_filePath + material.texNormalPath + textureExt;
            blinnPhongMaterial->m_hasTexNormal = true;
        }
        else
        { blinnPhongMaterial->m_hasTexNormal = false; }

        meshPart->setMaterial( blinnPhongMaterial );
        m_fileData->m_geometryData.push_back( std::move( meshPart ) );
    }

#if 0
    m_VertexBuffer.Create(L"VertexBuffer", m_Header.vertexDataByteSize / m_VertexStride, m_VertexStride, m_pVertexData);
    m_IndexBuffer.Create(L"IndexBuffer", m_Header.indexDataByteSize / sizeof(uint16_t), sizeof(uint16_t), m_pIndexData);
    delete [] m_pVertexData;
    m_pVertexData = nullptr;
    delete [] m_pIndexData;
    m_pIndexData = nullptr;

    m_VertexBufferDepth.Create(L"VertexBufferDepth", m_Header.vertexDataByteSizeDepth / m_VertexStrideDepth, m_VertexStrideDepth, m_pVertexDataDepth);
    m_IndexBufferDepth.Create(L"IndexBufferDepth", m_Header.indexDataByteSize / sizeof(uint16_t), sizeof(uint16_t), m_pIndexDataDepth);
    delete [] m_pVertexDataDepth;
    m_pVertexDataDepth = nullptr;
    delete [] m_pIndexDataDepth;
    m_pIndexDataDepth = nullptr;

    LoadTextures();
#endif
    ok = true;

h3d_load_fail:

    if ( EOF == fclose( file ) ) ok = false;

    return ok;
}

void H3DModel::reset() {
    delete[] m_pMesh;
    m_pMesh            = nullptr;
    m_Header.meshCount = 0;

    delete[] m_pMaterial;
    m_pMaterial            = nullptr;
    m_Header.materialCount = 0;

    delete[] m_pVertexData;
    delete[] m_pIndexData;
    delete[] m_pVertexDataDepth;
    delete[] m_pIndexDataDepth;

    m_pVertexData                    = nullptr;
    m_Header.vertexDataByteSize      = 0;
    m_pIndexData                     = nullptr;
    m_Header.indexDataByteSize       = 0;
    m_pVertexDataDepth               = nullptr;
    m_Header.vertexDataByteSizeDepth = 0;
    m_pIndexDataDepth                = nullptr;

    // ReleaseTextures();

    m_Header.boundingBox.min = Math::Vec3( 0.0f );
    m_Header.boundingBox.max = Math::Vec3( 0.0f );
}
} // namespace H3D
