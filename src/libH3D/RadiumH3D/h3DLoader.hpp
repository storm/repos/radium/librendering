//
// Created by Mathias Paulin on 22/09/2020.
//
#pragma once
#include <RadiumH3D/h3DMacros.hpp>

#include <Core/Asset/FileLoaderInterface.hpp>

namespace Ra::Core::Asset {
class FileData;
} // namespace Ra::Core::Asset

namespace H3D {

class h3D_LIBRARY_API H3DLoader : public Ra::Core::Asset::FileLoaderInterface
{
  public:
    H3DLoader();

    ~H3DLoader() override;

    /** Radium Loader interface
     *
     * @return {"gltf", "glb"}
     */
    [[nodiscard]] std::vector<std::string> getFileExtensions() const override;

    /**
     * check if an extension is managed by the loader
     * @param extension
     * @return true if extension is gltf or glb
     */
    [[nodiscard]] bool handleFileExtension( const std::string& extension ) const override;

    /** Try to load file, returns nullptr in case of failure
     *
     * @param filename the file to load
     * @return the File data representing the gltf scene, nullptr if loading failed
     */
    Ra::Core::Asset::FileData* loadFile( const std::string& filename ) override;

    /** Unique name of the loader
     *
     * @return "glTF 2.0"
     */
    [[nodiscard]] std::string name() const override;
};

} // namespace H3D
