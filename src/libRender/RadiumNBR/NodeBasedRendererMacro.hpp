#pragma once
#include <Core/CoreMacros.hpp>
/// Defines the correct macro to export dll symbols.
#if defined NBR_EXPORTS
#    define NodeBasedRenderer_LIBRARY_API DLL_EXPORT
#else
#    define NodeBasedRenderer_LIBRARY_API DLL_IMPORT
#endif
