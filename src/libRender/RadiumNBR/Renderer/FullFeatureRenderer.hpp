#pragma once
#include <RadiumNBR/NodeBasedRendererMacro.hpp>
#include <RadiumNBR/RenderPass.hpp>
#include <RadiumNBR/SphereSampler.hpp>

#include <RadiumNBR/NodeBasedRenderer.hpp>

namespace globjects {
class Framebuffer;
}

namespace Ra::Engine::Data {
class Texture;
} // namespace Ra::Engine::Data

namespace RadiumNBR {
class ClearPass;
class GeomPrePass;
class AccessibilityBufferPass;
class EmissivityPass;
class EnvLightPass;
class LocalLightPass;
class VolumeLightingPass;
class TransparencyPass;
class WireframePass;

/** Advanced renderer for the Radium Engine
 */
class NodeBasedRenderer_LIBRARY_API FullFeatureRenderer final : public NodeBasedRenderer
{

  public:
    FullFeatureRenderer();
    ~FullFeatureRenderer() override;

    [[nodiscard]] std::string getRendererName() const override { return "Full Featured Renderer"; }

    void setEnvMap( const std::string& files );
    void showEnvMap( bool state );
    void setEnvStrength( int s );

    // TODO : define a way to configure each passes without adding here specifi methods
    // TODO the following might be removed in the future
    Scalar getAoRadius() const;
    void setAoRadius( Scalar r );
    int getAoSamplingDensity() const;
    void setAoSamplingDensity( int d );

    void wireframeMode( bool status );

  protected:
    void initializeInternal() override;
    void updateStepInternal( const Ra::Engine::Data::ViewingParameters& renderData ) override;

  private:
    void initPasses();

    enum FullFeaturedRendererPasses : int {
        CLEAR_PASS = 0,
        Z_PASS,
        ACCESSIBILITY_PASS,
        EMISSIVITY_PASS,
        ENVMAP_LIGHTING_OPAQUE_PASS,
        LIGHTING_OPAQUE_PASS,
        LIGHTING_TRANSPARENT_PASS,
        LIGHTING_VOLUME_PASS,
        WIREFRAME_PASS,
        NUM_PASSES,
        DEFAULT_PASS = LIGHTING_OPAQUE_PASS
    };

  protected:
    /// Subset of the objects that are transparent and need special rendering
    std::vector<RenderObjectPtr> m_transparentRenderObjects;

    /// Subset of the objects that are volumetric
    std::vector<RenderObjectPtr> m_volumetricRenderObjects;

    /// The ambiant occlusion pass
    std::shared_ptr<AccessibilityBufferPass> m_aoPass;
    // The sampling method of the sphere sampler
    SphereSampler::SamplingMethod m_aoSamplingMethod{SphereSampler::SamplingMethod::HAMMERSLEY};
    // The number of points for the sampler
    int m_aoSamplingPoints{64};

    /// clear the final ouput image
    std::shared_ptr<ClearPass> m_clearPass;

    /// The zprepass
    std::shared_ptr<GeomPrePass> m_zPrePass;

    /// The emissivity  pass
    std::shared_ptr<EmissivityPass> m_emissivityPass;

    /// The envlight  pass
    std::shared_ptr<EnvLightPass> m_envlightPass;

    /// The local lighting pass
    std::shared_ptr<LocalLightPass> m_locallightPass;

    /// The volume lighting pass
    std::shared_ptr<VolumeLightingPass> m_volumelightPass;

    /// The transparency (Order independant transparency) pass
    std::shared_ptr<TransparencyPass> m_transparencyPass;

    /// The transparency (Order independant transparency) pass
    std::shared_ptr<WireframePass> m_wireframePass;

    /// Is an envmap attached to the renderer
    bool m_hasEnvMap{false};
};

} // namespace RadiumNBR
