#include <RadiumNBR/NodeBasedRenderer.hpp>

#include <RadiumNBR/EnvMap.hpp>

namespace RadiumNBR {
class CustomAttribToColorPass;
class ClearPass;
class ImageProcessPass;

/**
 * This class parameterize the renderer just after the OpenGL system was initialized.
 * when a method of this controler is called, the OpenGL context of the drawing window is activated.
 */
class NodeBasedRenderer_LIBRARY_API VisualizationController
    : public RadiumNBR::NodeBasedRenderer::RenderControlFunctor
{
  public:
    /*
     * Called once : configure the renderer by adding passes and allocating controler resources
     */
    void configure( RadiumNBR::NodeBasedRenderer* renderer, int w, int h ) override;

    void update( const Ra::Engine::Data::ViewingParameters& ) override;

    void resize( int w, int h ) override;

    /// Set the custom glsl function for attrib management (vertex) and colorcomputation (fragment)
    void setAttribToColorFunc( const std::string& vertex_source,
                               const std::string& geometry_source,
                               const std::string& fragment_source );

    /// Get the glsl functions
    std::tuple<std::string&, std::string&, std::string&> getAttribToColorFunc();

    /// activate or deactivate the gamma-correction on the computed color
    inline void enablePostProcess( bool b ) {
        m_postProcess = b;
        m_renderer->enablePostProcess( b );
    }

    float getSplatSize();
    void setSplatSize( float s );

    void setEnvMap( const std::string& files );
    void showEnvMap( bool state );
    void setEnvStrength( int s );

    void exportVectorField( bool state );
    bool exportsVectorField() const;

  private:
    /// The custom pass if needed for modification
    std::shared_ptr<RadiumNBR::CustomAttribToColorPass> m_customPass;

    /// The clear pass for the background
    std::shared_ptr<RadiumNBR::ClearPass> m_clearPass;

    /// The image processing pass to render vector field
    std::shared_ptr<RadiumNBR::ImageProcessPass> m_imageProcessPass;


    /// TODO : this default are the same as in the CustomAttribToColorPass. Use them instead of redefining here
    std::string m_vertexFunction{"void outputCustomAttribs() {\n}\n"};
    std::string m_geometryFunction{"void propagateAttributes(){}\n"};
    std::string m_fragmentFunction{
        "\nvec4 computeCustomColor(Material mat, vec3 lightDir, vec3 viewDir, vec3 normal_world) { \n"
        "vec3 diffColor; \n"
        "vec3 specColor; \n"
        "getSeparateBSDFComponent( mat, getPerVertexTexCoord(), lightDir,  viewDir,\n"
        "vec3(0, 0, 1), diffColor, specColor );\n"
        "vec3 envd;\n"
        "vec3 envs;\n"
        "float r = getGGXRoughness(mat, getPerVertexTexCoord());\n"
        "int e = getEnvMapColors(r, normal_world, envd, envs);"
        "vec3 finalColor;\n"
        "if (e==1) { finalColor = diffColor*envd + specColor*envs; }\n"
        "else { finalColor = (diffColor + specColor) * max(lightDir.z, 0) \n"
        "       * lightContributionFrom(light, getWorldSpacePosition().xyz); }\n"
        "#ifdef EXPORT_VECTOR_FIELD\n"
        "out_vector_field = vec4(getWorldSpaceTangent()*0.5+0.5, 1);\n"
        "#endif\n"
        "return vec4( finalColor, 1); \n"
        "}\n"};

    RadiumNBR::NodeBasedRenderer* m_renderer;

    bool m_postProcess{true};

    /// Is an envmap attached to the renderer
    bool m_hasEnvMap{false};

    /// The Environment to used for skybox display
    std::shared_ptr<EnvMap> m_envmap{nullptr};
};

} // namespace RadiumNBR
