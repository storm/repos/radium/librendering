#pragma once
#include <RadiumNBR/NodeBasedRendererMacro.hpp>

#include <RadiumNBR/Gui/RendererPanel.hpp>
namespace RadiumNBR {
class NodeBasedRenderer;

NodeBasedRenderer_LIBRARY_API RadiumNBR::Gui::RendererPanel*
buildControllerGui( NodeBasedRenderer* renderer, const std::function<void()>& appUpdateCallback );

} // namespace RadiumNBR
