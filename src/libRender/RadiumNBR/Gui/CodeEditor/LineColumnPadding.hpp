#pragma once
#include <RadiumNBR/NodeBasedRendererMacro.hpp>

#include <QtCore>

namespace RadiumNBR::Gui {

///
///  @file      QLineColumnPadding.hpp
///  @author    Nicolas Kogler
///  @date      October 5th, 2016
///  @class     QLineColumnPadding
///  @brief     Defines a padding in the right- and left direction.
///
class NodeBasedRenderer_LIBRARY_API QLineColumnPadding
{
  public:
    ///
    ///  @fn    Default constructor
    ///  @brief Initializes a new instance of QLineColumnPadding.
    ///
    QLineColumnPadding();

    ///
    ///  @fn    Constructor
    ///  @brief Constructs a padding out of a QSize.
    ///  @param size Size representing left and right padding
    ///
    QLineColumnPadding( const QSize& size );

    ///
    ///  @fn    Constructor
    ///  @brief Initializes a new instance of QLineColumnPadding.
    ///  @param left Initial left padding
    ///  @param right Initial right padding
    ///
    QLineColumnPadding( quint32 left, quint32 right );

    ///
    ///  @fn     Destructor
    ///  @brief  Frees all resources allocated by QLineColumnPadding.
    ///
    ~QLineColumnPadding();

    ///
    ///  @fn      left : const
    ///  @brief   Retrieves the padding in the left direction.
    ///  @returns the left padding.
    ///
    quint32 left() const;

    ///
    ///  @fn      right : const
    ///  @brief   Retrieves the padding in the right direction.
    ///  @returns the right padding.
    ///
    quint32 right() const;

    ///
    ///  @fn    setLeft
    ///  @brief Specifies the left padding
    ///  @param left Left padding, in pixels
    ///
    void setLeft( quint32 left );

    ///
    ///  @fn    setRight
    ///  @brief Specifies the right padding
    ///  @param left Right padding, in pixels
    ///
    void setRight( quint32 right );

  private:
    //
    // Private class members
    //
    quint32 m_Left;
    quint32 m_Right;
};
} // namespace RadiumNBR::Gui
