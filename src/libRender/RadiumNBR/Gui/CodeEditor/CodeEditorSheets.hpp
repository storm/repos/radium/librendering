#pragma once
#include <RadiumNBR/NodeBasedRendererMacro.hpp>

#include <RadiumNBR/Gui/CodeEditor/CodeEditorDesign.hpp>

namespace RadiumNBR::Gui {

///
///  @file      QCodeEditorSheets.hpp
///  @author    Nicolas Kogler
///  @date      October 7th, 2016
///  @def       CSS_Editor_Widget
///  @brief     Stylesheet for the editor widget.
///
const char CSS_Editor_Widget[] = {"QPlainTextEdit {"
                                  "   border-top: %tpx solid #%c;"
                                  "   border-right: %rpx solid #%c;"
                                  "   border-bottom: %bpx solid #%c;"
                                  "   border-left: %lpx solid #%c;"
                                  "}"};

///
///  @file      QCodeEditorSheets.hpp
///  @author    Nicolas Kogler
///  @date      October 6th, 2016
///  @def       CSS_Popup_Widget
///  @brief     Stylesheet for the popup widget.
///
const char CSS_Popup_Widget[] = {"QListView {"
                                 "   border-top: %tpx solid #%border;"
                                 "   border-left: %lpx solid #%border;"
                                 "   border-right: %rpx solid #%border;"
                                 "   border-bottom: %bpx solid #%border;"
                                 "   background-color: #%back;"
                                 "   color: #%text;"
                                 "   outline: %focus;"
                                 "}"
                                 "QListView::item:selected {"
                                 "   padding: -1px;"
                                 "   border: 1px solid #%selbrd;"
                                 "   background-color: #%selback;"
                                 "   color: #%text;"
                                 "}"
                                 "QListView::item:!selected:hover {"
                                 "   background: transparent;"
                                 "}"};

///
///  @file      QCodeEditorSheets.hpp
///  @author    Nicolas Kogler
///  @date      October 6th, 2016
///  @class     QCodeEditorSheets
///
class QCodeEditorSheets
{
  public:
    ///
    ///  @fn      border
    ///  @brief   Retrieves the style-sheet for the border.
    ///  @param   design Current code editor design
    ///  @returns the style-sheet for the border.
    ///
    static QString border( const QCodeEditorDesign& design );
};

///
///  @file      QCodeEditorSheets.hpp
///  @author    Nicolas Kogler
///  @date      October 6th, 2016
///  @class     QCodeEditorPopupSheets
///
class QCodeEditorPopupSheets
{
  public:
    ///
    ///  @fn      hover
    ///  @brief   Retrieves the style-sheet for mouse-hover.
    ///  @param   design Current code editor design
    ///  @returns the style-sheet for mouse-hovers.
    ///
    static QString hover( const QCodeEditorDesign& design );

    ///
    ///  @fn      press
    ///  @brief   Retrieves the style-sheet for mouse-press.
    ///  @param   design Current code editor design
    ///  @returns the style-sheet for mouse-presses.
    ///
    static QString press( const QCodeEditorDesign& design );
};
} // namespace RadiumNBR::Gui
