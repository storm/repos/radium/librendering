#pragma once
#include <RadiumNBR/NodeBasedRendererMacro.hpp>

//
//  Included headers
//
#include <QListView>
#include <RadiumNBR/Gui/CodeEditor/CodeEditor.hpp>

namespace RadiumNBR::Gui {

///
///  @file      QCodeEditorPopup.hpp
///  @author    Nicolas Kogler
///  @date      October 6th, 2016
///  @class     QCodeEditorPopup
///  @brief
///
class NodeBasedRenderer_LIBRARY_API QCodeEditorPopup : public QListView
{
  public:
    ///
    ///  @fn    Constructor
    ///  @brief Initializes a new instance of QCodeEditorPopup.
    ///
    QCodeEditorPopup( QCodeEditor* parent );

    ///
    ///  @fn    Destructor
    ///  @brief Frees all resources allocated by QCodeEditorPopup.
    ///
    ~QCodeEditorPopup();

  protected:
    ///
    /// @fn      viewportEvent
    /// @brief   Intercepts events for the QCodeEditor viewport.
    /// @param   event Event of unknown type
    /// @returns true to indicate we have handled the event ourselves.
    ///
    bool viewportEvent( QEvent* event ) Q_DECL_OVERRIDE;

  private:
    //
    // Private class members
    //
    QString m_StyleSheetNormal; ///< Holds the stylesheet on mouse-hover
    QString m_StyleSheetPress;  ///< Holds the stylesheet on mouse-press
    QCodeEditor* m_Parent;      ///< Holds the parent for design-property access
};
} // namespace RadiumNBR::Gui
