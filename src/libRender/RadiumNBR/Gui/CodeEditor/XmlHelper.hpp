#pragma once
#include <RadiumNBR/NodeBasedRendererMacro.hpp>

//
//  Included headers
//
#include <QMargins>
#include <QXmlStreamReader>

namespace RadiumNBR::Gui {

///
///  @file      XmlHelper.hpp
///  @author    Nicolas Kogler
///  @date      October 18th, 2016
///  @brief     Declares several functions for parsing XML elements.
///

///
///  @fn      readBool
///  @brief   Reads an XML element and converts it to a boolean.
///  @param   reader Current XML reader
///  @returns true if element is string "true", otherwise false.
///
extern bool readBool( QXmlStreamReader* reader );

///
///  @fn      readColor
///  @brief   Reads an XML element and converts it to a color.
///  @param   reader Current XML reader
///  @returns the color representing the inline text.
///
/// Supported:
/// Hexadecimal notation (e.g. '#abcdef')
/// Color strings (e.g. 'red')
/// RGBA notation (e.g. 'rgba(r, g, b, a)
///
extern QColor readColor( QXmlStreamReader* reader );

///
///  @fn      readKeywords
///  @brief   Reads an XML element and converts it to a list of strings.
///  @param   reader Current XML reader
///  @returns all the words separated by whitespace
///
extern QStringList readKeywords( QXmlStreamReader* reader );

///
///  @fn      readMargin
///  @brief   Reads a margin with 4 values.
///  @param   reader Current XML reader
///  @returns a QMargins structure.
///
extern QMargins readMargin( QXmlStreamReader* reader );

///
///  @fn      readSize
///  @brief   Reads a size with 2 values.
///  @param   reader Current XML reader
///  @returns a QSize structure.
extern QSize readSize( QXmlStreamReader* reader );

///
///  @fn      readFont
///  @brief   Reads a font structure from the XML reader.
///  @param   reader Current XML reader
///  @param   def Default font, in case no family specified
///  @returns a QFont structure.
///
extern QFont readFont( QXmlStreamReader* reader, const QFont& def );
} // namespace RadiumNBR::Gui
