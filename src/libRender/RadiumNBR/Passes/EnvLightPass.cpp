#include <RadiumNBR/Passes/EnvLightPass.hpp>
#define PASSES_LOG
#ifdef PASSES_LOG
#    include <Core/Utils/Log.hpp>
using namespace Ra::Core::Utils; // log
#endif

#include <Engine/Data/Material.hpp>
#include <Engine/Data/ShaderConfigFactory.hpp>
#include <Engine/Data/ShaderProgramManager.hpp>
#include <Engine/Data/Texture.hpp>
#include <Engine/Rendering/RenderObject.hpp>

#include <globjects/Framebuffer.h>

namespace RadiumNBR {
using namespace gl;

static const GLenum buffers[] = {GL_COLOR_ATTACHMENT0};

EnvLightPass::EnvLightPass( const std::vector<RenderObjectPtr>* objectsToRender,
                            const Ra::Core::Utils::Index& idx ) :
    RenderPass( "Environment lighting", idx, objectsToRender ) {}

EnvLightPass::~EnvLightPass() = default;

bool EnvLightPass::initializePass( size_t /* width */,
                                   size_t /* height */,
                                   Ra::Engine::Data::ShaderProgramManager* shaderMngr ) {
    m_shaderMngr = shaderMngr;
    m_fbo        = std::make_unique<globjects::Framebuffer>();
    // Define a neutral envmap
    float** neutralEnv = new float*[6];
    for ( int imgIdx = 0; imgIdx < 6; ++imgIdx )
    {
        neutralEnv[imgIdx]    = new float[4];
        neutralEnv[imgIdx][0] = neutralEnv[imgIdx][1] = neutralEnv[imgIdx][2] = 0.f;
        neutralEnv[imgIdx][3]                                                 = 0.f;
    }
    Ra::Engine::Data::TextureParameters params{"neutralEnv",
                                               GL_TEXTURE_CUBE_MAP,
                                               1,
                                               1,
                                               1,
                                               GL_RGBA,
                                               GL_RGBA,
                                               GL_FLOAT,
                                               GL_CLAMP_TO_EDGE,
                                               GL_CLAMP_TO_EDGE,
                                               GL_CLAMP_TO_EDGE,
                                               GL_LINEAR,
                                               GL_LINEAR,
                                               (void**)( neutralEnv )};
    return true;
}

void EnvLightPass::setInputs( const SharedTextures& depthBuffer,
                              const SharedTextures& ambientOcclusion ) {
    addImportedTextures( {"EnvLight::Depth", depthBuffer.second} );
    addImportedTextures( {"EnvLight::AmbOcc", ambientOcclusion.second} );
    m_passParams.addParameter( "amb_occ_sampler", ambientOcclusion.second.get() );
}

void EnvLightPass::setOutput( const SharedTextures& colorBuffer ) {
    m_outputTexture = colorBuffer;
}

bool EnvLightPass::update() {

    return true;
}

void EnvLightPass::resize( size_t width, size_t height ) {
    // Only resize the owned textures. Imported one are resized by their owner
    for ( auto& t : m_localTextures )
    {
        t.second->resize( width, height );
    }

    for ( auto& t : m_sharedTextures )
    {
        t.second->resize( width, height );
    }

    m_fbo->bind();
    m_fbo->attachTexture( GL_DEPTH_ATTACHMENT, m_importedTextures["EnvLight::Depth"]->texture() );
    m_fbo->attachTexture( GL_COLOR_ATTACHMENT0, m_outputTexture.second->texture() );
#ifdef PASSES_LOG
    if ( m_fbo->checkStatus() != GL_FRAMEBUFFER_COMPLETE )
    { LOG( logERROR ) << "FBO Error (EnvLightPass::resize): " << m_fbo->checkStatus(); }
#endif
    // finished with fbo, undbind to bind default
    globjects::Framebuffer::unbind();
}

void EnvLightPass::execute( const Ra::Engine::Data::ViewingParameters& viewParams ) const {
    using ClearColor = Ra::Core::Utils::Color;
    if ( m_envmap == nullptr ) { return; }
    m_fbo->bind();
    // only draw into 1 buffers (Color)
    GL_ASSERT( glDrawBuffers( 1, buffers ) );
    GL_ASSERT( glDepthMask( GL_FALSE ) );
    GL_ASSERT( glColorMask( GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE ) );
    GL_ASSERT( glEnable( GL_DEPTH_TEST ) );
    GL_ASSERT( glDepthFunc( GL_EQUAL ) );
    GL_ASSERT( glEnable( GL_BLEND ) );
    GL_ASSERT( glBlendFunc( GL_ONE, GL_ONE ) );

    m_passParams.addParameter( "envStrength", m_envmap->getEnvStrength() );

    for ( const auto& ro : *m_objectsToRender )
    {
        ro->render( m_passParams, viewParams, passIndex() );
    }

    // Beware of the state left by the pass
    GL_ASSERT( glDisable( GL_BLEND ) );
}

bool EnvLightPass::buildRenderTechnique( const Ra::Engine::Rendering::RenderObject* ro,
                                         Ra::Engine::Rendering::RenderTechnique& rt ) const {
    std::string resourcesRootDir = getResourcesDir();
    auto mat = const_cast<Ra::Engine::Rendering::RenderObject*>( ro )->getMaterial();
    // Volumes are not used in EnvLightPass
    if ( mat->getMaterialAspect() == Ra::Engine::Data::Material::MaterialAspect::MAT_DENSITY )
    { return false; }
    if ( auto cfg = Ra::Engine::Data::ShaderConfigurationFactory::getConfiguration(
             {"EnvLightPass::" + mat->getMaterialName()} ) )
    { rt.setConfiguration( *cfg, passIndex() ); }
    else
    {
        // Build the shader configuration
        Ra::Engine::Data::ShaderConfiguration theConfig{
            {"EnvLightPass::" + mat->getMaterialName()},
            resourcesRootDir + "Shaders/EnvLightPass/envlightpass.vert.glsl",
            resourcesRootDir + "Shaders/EnvLightPass/envlightpass.frag.glsl"};
        // add the material interface to the fragment shader
        theConfig.addInclude( "\"" + mat->getMaterialName() + ".glsl\"",
                              Ra::Engine::Data::ShaderType::ShaderType_FRAGMENT );
        theConfig.addProperty( mat->getMaterialName() );
        LOG( logINFO ) << "Added define : " << mat->getMaterialName();
        // Add to the ShaderConfigManager
        Ra::Engine::Data::ShaderConfigurationFactory::addConfiguration( theConfig );
        // Add to the RenderTechniq
        rt.setConfiguration( theConfig, passIndex() );
    }
    rt.setParametersProvider( mat, passIndex() );
    return true;
}

void EnvLightPass::setEnvMap( std::shared_ptr<EnvMap> envmp ) {
    m_envmap = envmp;
    if ( m_envmap != nullptr )
    {
        // Set the envmap parameter once.
        // Warning : there is no way to remove parameters. Might segfault if this is not consistent
        // with rendering usage
        m_passParams.addParameter( "redShCoeffs", m_envmap->getShMatrix( 0 ) );
        m_passParams.addParameter( "greenShCoeffs", m_envmap->getShMatrix( 1 ) );
        m_passParams.addParameter( "blueShCoeffs", m_envmap->getShMatrix( 2 ) );
        m_passParams.addParameter( "envTexture", m_envmap->getEnvTexture() );
        int numLod = std::log2(
            std::min( m_envmap->getEnvTexture()->width(), m_envmap->getEnvTexture()->height() ) );
        m_passParams.addParameter( "numLod", numLod );
    }
}

} // namespace RadiumNBR
