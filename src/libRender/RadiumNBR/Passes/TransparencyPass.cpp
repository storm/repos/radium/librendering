#include <RadiumNBR/Passes/TransparencyPass.hpp>

#ifdef PASSES_LOG
#    include <Core/Utils/Log.hpp>
using namespace Ra::Core::Utils; // log
#endif

#include <Core/Geometry/MeshPrimitives.hpp>
#include <Engine/Data/Material.hpp>
#include <Engine/Data/Mesh.hpp>
#include <Engine/Data/ShaderConfigFactory.hpp>
#include <Engine/Data/ShaderProgramManager.hpp>
#include <Engine/Data/Texture.hpp>
#include <Engine/Rendering/RenderObject.hpp>

#include <Engine/Scene/DefaultLightManager.hpp>

#include <globjects/Framebuffer.h>

namespace RadiumNBR {
using namespace gl;

static const GLenum buffers[] = {GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1};

TransparencyPass::TransparencyPass( const std::vector<RenderObjectPtr>* objectsToRender,
                                    const Ra::Core::Utils::Index& idx ) :
    RenderPass( "Order Independant Transparency", idx, objectsToRender ) {}

TransparencyPass::~TransparencyPass() = default;

bool TransparencyPass::initializePass( size_t width,
                                       size_t height,
                                       Ra::Engine::Data::ShaderProgramManager* shaderMngr ) {
    m_shaderMngr = shaderMngr;
    m_fbo        = std::make_unique<globjects::Framebuffer>();
    m_oitFbo     = std::make_unique<globjects::Framebuffer>();

    Ra::Engine::Data::TextureParameters texparams;
    texparams.width          = width;
    texparams.height         = height;
    texparams.target         = GL_TEXTURE_2D;
    texparams.minFilter      = GL_LINEAR;
    texparams.magFilter      = GL_LINEAR;
    texparams.internalFormat = GL_RGBA32F;
    texparams.format         = GL_RGBA;
    texparams.type           = GL_FLOAT;
    texparams.name           = "Transparency::Accum";
    m_sharedTextures.insert(
        {texparams.name, std::make_shared<Ra::Engine::Data::Texture>( texparams )} );
    texparams.name = "Transparency::Revealage";
    m_sharedTextures.insert(
        {texparams.name, std::make_shared<Ra::Engine::Data::Texture>( texparams )} );

    // The compositer caller
    Ra::Core::Geometry::TriangleMesh mesh =
        Ra::Core::Geometry::makeZNormalQuad( Ra::Core::Vector2( -1.f, 1.f ) );
    auto qm = std::make_unique<Ra::Engine::Data::Mesh>( "caller" );
    qm->loadGeometry( std::move( mesh ) );
    m_quadMesh = std::move( qm );
    m_quadMesh->updateGL();

    const std::string composeVertexShader{"layout (location = 0) in vec3 in_position;\n"
                                          "out vec2 varTexcoord;\n"
                                          "void main()\n"
                                          "{\n"
                                          "  gl_Position = vec4(in_position, 1.0);\n"
                                          "  varTexcoord = (in_position.xy + 1.0) * 0.5;\n"
                                          "}\n"};
    const std::string composeFragmentShader{
        "in vec2 varTexcoord;\n"
        "out vec4 f_Color;\n"
        "uniform sampler2D u_OITSumColor;\n"
        "uniform sampler2D u_OITSumWeight;\n"
        "void main() {\n"
        "   float r = texture( u_OITSumWeight, varTexcoord ).r;\n"
        "   if ( r >= 1.0 ) { discard; }\n"
        "   vec4 accum = texture( u_OITSumColor, varTexcoord );\n"
        "   vec3 avg_color = accum.rgb / max( accum.a, 0.00001 );\n"
        "   f_Color = vec4( avg_color, r );\n"
        "}"};
    Ra::Engine::Data::ShaderConfiguration config{"ComposeTransparency"};
    config.addShaderSource( Ra::Engine::Data::ShaderType::ShaderType_VERTEX, composeVertexShader );
    config.addShaderSource( Ra::Engine::Data::ShaderType::ShaderType_FRAGMENT,
                            composeFragmentShader );
    auto added = shaderMngr->addShaderProgram( config );
    if ( added ) { m_shader = added.value(); }
    else
    { return false; }

    return true;
}

void TransparencyPass::setInputs( const SharedTextures& depthBuffer,
                                  const SharedTextures& ambientOcclusion ) {
    addImportedTextures( {"Transparency::Depth", depthBuffer.second} );
    addImportedTextures( {"Transparency::AmbOcc", ambientOcclusion.second} );
}

void TransparencyPass::setOutput( const SharedTextures& colorBuffer ) {
    m_outputTexture = colorBuffer;
}

bool TransparencyPass::update() {
    return true;
}

void TransparencyPass::resize( size_t width, size_t height ) {
    // Only resize the owned textures. Imported one are resized by their owner
    for ( auto& t : m_localTextures )
    {
        t.second->resize( width, height );
    }

    for ( auto& t : m_sharedTextures )
    {
        t.second->resize( width, height );
    }

    m_fbo->bind();
    m_fbo->attachTexture( GL_DEPTH_ATTACHMENT,
                          m_importedTextures["Transparency::Depth"]->texture() );
    m_fbo->attachTexture( GL_COLOR_ATTACHMENT0, m_outputTexture.second->texture() );
#ifdef PASSES_LOG
    if ( m_fbo->checkStatus() != GL_FRAMEBUFFER_COMPLETE )
    { LOG( logERROR ) << "FBO Error (TransparencyPass::m_fbo): " << m_fbo->checkStatus(); }
#endif
    m_oitFbo->bind();
    m_oitFbo->attachTexture( GL_DEPTH_ATTACHMENT,
                             m_importedTextures["Transparency::Depth"]->texture() );
    m_oitFbo->attachTexture( GL_COLOR_ATTACHMENT0,
                             m_sharedTextures["Transparency::Accum"]->texture() );
    m_oitFbo->attachTexture( GL_COLOR_ATTACHMENT1,
                             m_sharedTextures["Transparency::Revealage"]->texture() );
#ifdef PASSES_LOG
    if ( m_oitFbo->checkStatus() != GL_FRAMEBUFFER_COMPLETE )
    { LOG( logERROR ) << "FBO Error (TransparencyPass::m_oitFbo) : " << m_oitFbo->checkStatus(); }
#endif
    // finished with fbo, undbind to bind default
    globjects::Framebuffer::unbind();
}

void TransparencyPass::execute( const Ra::Engine::Data::ViewingParameters& viewParams ) const {
    if ( m_objectsToRender->empty() ) { return; }

    using ClearColor             = Ra::Core::Utils::Color;
    static const auto clearZeros = ClearColor::Black();
    static const auto clearOnes  = ClearColor::White();

    m_oitFbo->bind();

    GL_ASSERT( glDrawBuffers( 2, buffers ) );
    GL_ASSERT( glClearBufferfv( GL_COLOR, 0, clearZeros.data() ) );
    GL_ASSERT( glClearBufferfv( GL_COLOR, 1, clearOnes.data() ) );
    GL_ASSERT( glDepthMask( GL_FALSE ) );
    GL_ASSERT( glDepthFunc( GL_LEQUAL ) );
    GL_ASSERT( glEnable( GL_DEPTH_TEST ) );

    GL_ASSERT( glEnable( GL_BLEND ) );
    GL_ASSERT( glBlendEquation( GL_FUNC_ADD ) );
    GL_ASSERT( glBlendFunci( 0, GL_ONE, GL_ONE ) );
    GL_ASSERT( glBlendFunci( 1, GL_ZERO, GL_ONE_MINUS_SRC_ALPHA ) );

    /// Todo what do we do wit AO ? no need ?
    // const auto tao = m_importedTextures.find( "Transparency::AmbOcc" );

    if ( m_lightmanager->count() > 0 )
    {
        for ( size_t i = 0; i < m_lightmanager->count(); ++i )
        {
            Ra::Engine::Data::RenderParameters passParams;
            // passParams.addParameter( "amb_occ_sampler", tao->second.get() );
            const auto l = m_lightmanager->getLight( i );
            l->getRenderParameters( passParams );

            for ( const auto& ro : *m_objectsToRender )
            {
                ro->render( passParams, viewParams, passIndex() );
            }
        }
    }
#ifdef PASSES_LOG
    else
    { LOG( logINFO ) << "Transparency : no light sources, unable to render"; }
#endif
    m_fbo->bind();
    GL_ASSERT( glDrawBuffers( 1, buffers ) );
    GL_ASSERT( glDisable( GL_DEPTH_TEST ) );
    GL_ASSERT( glBlendFunc( GL_ONE_MINUS_SRC_ALPHA, GL_SRC_ALPHA ) );
    {
        auto accumTex     = m_sharedTextures.find( "Transparency::Accum" );
        auto revealageTex = m_sharedTextures.find( "Transparency::Revealage" );

        m_shader->bind();
        m_shader->setUniform( "u_OITSumColor", accumTex->second.get(), 0 );
        m_shader->setUniform( "u_OITSumWeight", revealageTex->second.get(), 1 );

        m_quadMesh->render( m_shader );
    }
    GL_ASSERT( glEnable( GL_DEPTH_TEST ) );
    // Beware of the state left by the pass
    GL_ASSERT( glDisable( GL_BLEND ) );
}

bool TransparencyPass::buildRenderTechnique( const Ra::Engine::Rendering::RenderObject* ro,
                                             Ra::Engine::Rendering::RenderTechnique& rt ) const {
    // Only transparent objects matters
    if ( !ro->isTransparent() ) { return false; }

    auto mat = const_cast<Ra::Engine::Rendering::RenderObject*>( ro )->getMaterial();

    if ( auto cfg = Ra::Engine::Data::ShaderConfigurationFactory::getConfiguration(
             {"TransparencyPass::" + mat->getMaterialName()} ) )
    { rt.setConfiguration( *cfg, passIndex() ); }
    else
    {
        std::string resourcesRootDir = getResourcesDir();
        // Build the shader configuration
        Ra::Engine::Data::ShaderConfiguration theConfig{
            {"TransparencyPass::" + mat->getMaterialName()},
            resourcesRootDir + "Shaders/TransparencyPass/oitpass.vert.glsl",
            resourcesRootDir + "Shaders/TransparencyPass/oitpass.frag.glsl"};
        // add the material interface to the fragment shader
        theConfig.addInclude( "\"" + mat->getMaterialName() + ".glsl\"",
                              Ra::Engine::Data::ShaderType::ShaderType_FRAGMENT );
        // Add to the ShaderConfigManager
        Ra::Engine::Data::ShaderConfigurationFactory::addConfiguration( theConfig );
        // Add to the RenderTechniq
        rt.setConfiguration( theConfig, passIndex() );
    }
    rt.setParametersProvider( mat, passIndex() );
    return true;
}

void TransparencyPass::setLightManager( const Ra::Engine::Scene::LightManager* lm ) {
    m_lightmanager = lm;
}
} // namespace RadiumNBR
