#include <RadiumNBR/Passes/ImageProcessPass.hpp>

#ifdef PASSES_LOG
#    include <Core/Utils/Log.hpp>
using namespace Ra::Core::Utils; // log
#endif

#include <Core/Geometry/MeshPrimitives.hpp>

#include <Engine/Data/Mesh.hpp>
#include <Engine/Data/ShaderConfigFactory.hpp>
#include <Engine/Data/ShaderProgramManager.hpp>
#include <Engine/Data/Texture.hpp>
#include <Engine/Data/ViewingParameters.hpp>
#include <Engine/RadiumEngine.hpp>

#include <globjects/Framebuffer.h>

namespace RadiumNBR {
using namespace gl;

static const GLenum buffer = { GL_COLOR_ATTACHMENT0 };

ImageProcessPass::ImageProcessPass( const Ra::Core::Utils::Index& idx ) :
    RenderPass( "Image processing pass", idx, nullptr ) {}

ImageProcessPass::~ImageProcessPass() = default;


bool ImageProcessPass::initializePass( size_t width,
                                       size_t height,
                                       Ra::Engine::Data::ShaderProgramManager* shaderMngr ) {
    // Initialize the fbo
    m_fbo     = std::make_unique<globjects::Framebuffer>();

    Ra::Engine::Data::TextureParameters texparams;
    texparams.width     = width;
    texparams.height    = height;
    texparams.target    = GL_TEXTURE_2D;
    texparams.minFilter = GL_LINEAR;
    texparams.magFilter = GL_LINEAR;
    texparams.internalFormat = GL_RGBA32F;
    texparams.format         = GL_RGBA;
    texparams.type = GL_FLOAT;
    texparams.name = "ImageProcess::Result";
    m_sharedTextures.insert(
        {texparams.name, std::make_shared<Ra::Engine::Data::Texture>( texparams )} );

    // The shader caller
    Ra::Core::Geometry::TriangleMesh mesh =
        Ra::Core::Geometry::makeZNormalQuad( Ra::Core::Vector2( -1.f, 1.f ) );
    auto qm = std::make_unique<Ra::Engine::Data::Mesh>( "caller" );
    qm->loadGeometry( std::move( mesh ) );
    m_quadMesh = std::move( qm );
    m_quadMesh->updateGL();

    // The shader
    // TODO, based on the same approach than in CustomAttribToColorPass,
    //  in the future, this shader might be user-defined
    std::string resourcesRootDir = getResourcesDir();
    auto added                   = shaderMngr->addShaderProgram(
        {{"ImageProcess(LIC)"},
         resourcesRootDir + "Shaders/ImageProcessingPass/ImageProcess.vert.glsl",
         resourcesRootDir + "Shaders/ImageProcessingPass/ImageProcess.frag.glsl"} );
    if ( added ) { m_shader = added.value(); }
    else
    { return false; }

    return true;
}


bool ImageProcessPass::update() {
    // nothing to do
    return true;
}

void ImageProcessPass::resize( size_t width, size_t height ) {
    for ( auto& t : m_sharedTextures )
    {
        t.second->resize( width, height );
    }

    m_fbo->bind();
    m_fbo->attachTexture( GL_COLOR_ATTACHMENT0, m_sharedTextures["ImageProcess::Result"]->texture() );
#ifdef PASSES_LOG
    if ( m_fbo->checkStatus() != GL_FRAMEBUFFER_COMPLETE )
    { LOG( logERROR ) << "FBO Error (GeomPrePass::resize): " << m_fbo->checkStatus(); }
#endif

    globjects::Framebuffer::unbind();
}

void ImageProcessPass::execute(
    const Ra::Engine::Data::ViewingParameters& viewParams ) const {
    m_fbo->bind();
    GL_ASSERT( glDrawBuffers( 1, &buffer ) );
    GL_ASSERT( glClearBufferfv( GL_COLOR, 0, Ra::Core::Utils::Color::White().data() ) );
    GL_ASSERT( glDisable( GL_DEPTH_TEST ) );
    GL_ASSERT( glDepthMask( GL_FALSE ) );

    m_shader->bind();
    Ra::Core::Matrix4 viewproj = viewParams.projMatrix * viewParams.viewMatrix;

    m_shader->setUniform( "transform.mvp", viewproj );
    m_shader->setUniform( "transform.proj", viewParams.projMatrix );
    m_shader->setUniform( "transform.view", viewParams.viewMatrix );

    const auto td = m_importedTextures.find( "ImageProcess::DepthSampler" );
    m_shader->setUniform( "depth_sampler", td->second.get(), 0 );
    const auto tc = m_importedTextures.find( "ImageProcess::ColorSampler" );
    m_shader->setUniform( "image_sampler", tc->second.get(), 1 );

    m_quadMesh->render( m_shader );
    GL_ASSERT( glEnable( GL_DEPTH_TEST ) );
    GL_ASSERT( glDepthMask( GL_TRUE ) );
    m_fbo->unbind();

}

/// These inputs must be computed before executing this pass
/// positions and normals must be computed in world space
void ImageProcessPass::setInputs( const SharedTextures& depthBuffer,
                                  const SharedTextures& colorBuffer ) {
    m_importedTextures["ImageProcess::DepthSampler"] = depthBuffer.second;
    m_importedTextures["ImageProcess::ColorSampler"]   = colorBuffer.second;
}

void ImageProcessPass::setOutput( const SharedTextures& colorBuffer ) {
    m_outputTexture = colorBuffer;
}

// Nothing to do, this pass is screen space
bool ImageProcessPass::buildRenderTechnique(
    const Ra::Engine::Rendering::RenderObject* ro,
    Ra::Engine::Rendering::RenderTechnique& rt ) const {
    return true;
};

}
