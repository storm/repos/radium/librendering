#pragma once
#include <RadiumNBR/RenderPass.hpp>

#include <Core/Utils/Color.hpp>
#include <Engine/Data/DisplayableObject.hpp>

namespace Ra::Engine::Data {
class ShaderProgram;
} // namespace Ra::Engine::Data

namespace globjects {
class Framebuffer;
}

namespace RadiumNBR {
/// Render pass that draws objects as wireframe over an existing framebuffer
class WireframePass : public RenderPass
{
  public:
    WireframePass( const std::vector<RenderObjectPtr>* objectsToRender,
                   const Ra::Core::Utils::Index& idx );
    ~WireframePass() override;

    bool buildRenderTechnique( const Ra::Engine::Rendering::RenderObject* ro,
                               Ra::Engine::Rendering::RenderTechnique& rt ) const override;
    bool initializePass( size_t width,
                         size_t height,
                         Ra::Engine::Data::ShaderProgramManager* shaderMngr ) override;
    bool update() override;
    void resize( size_t width, size_t height ) override;
    void execute( const Ra::Engine::Data::ViewingParameters& viewParams ) const override;

    /// Add the output colorBuffer
    void setOutput( const SharedTextures& colorBuffer );

    /// These inputs must be computed before executing this pass : depth buffer
    void setInputs( const SharedTextures& depthBuffer );

  private:
    /// The framebuffer used to render this pass
    std::unique_ptr<globjects::Framebuffer> m_fbo{nullptr};

    /// The Shader manager to use when building shaders
    Ra::Engine::Data::ShaderProgramManager* m_shaderMngr{nullptr};

    /// The color texture for output.Stored here for easy access.
    SharedTextures m_outputTexture;

    using WireMap = std::map<Ra::Engine::Rendering::RenderObject*,
                             std::shared_ptr<Ra::Engine::Data::Displayable>>;
    mutable WireMap m_wireframes;
    bool m_wireframeAa{true};
    Ra::Core::Utils::Color m_wireframeColor{Ra::Core::Utils::Color::White()};

    uint m_width{0};
    uint m_height{0};
};
} // namespace RadiumNBR
