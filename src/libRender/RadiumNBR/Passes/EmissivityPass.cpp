#include <RadiumNBR/Passes/EmissivityPass.hpp>

#ifdef PASSES_LOG
#    include <Core/Utils/Log.hpp>
using namespace Ra::Core::Utils; // log
#endif

#include <Engine/Data/Material.hpp>
#include <Engine/Data/ShaderConfigFactory.hpp>
#include <Engine/Data/ShaderProgramManager.hpp>
#include <Engine/Data/Texture.hpp>
#include <Engine/Rendering/RenderObject.hpp>

#include <globjects/Framebuffer.h>

namespace RadiumNBR {
using namespace gl;

static const GLenum buffers[] = {GL_COLOR_ATTACHMENT0};

EmissivityPass::EmissivityPass( const std::vector<RenderObjectPtr>* objectsToRender,
                                const Ra::Core::Utils::Index& idx ) :
    RenderPass( "Emissivity pass", idx, objectsToRender ) {}

EmissivityPass::~EmissivityPass() = default;

bool EmissivityPass::initializePass( size_t /* width */,
                                     size_t /* height */,
                                     Ra::Engine::Data::ShaderProgramManager* shaderMngr ) {
    m_shaderMngr = shaderMngr;
    m_fbo        = std::make_unique<globjects::Framebuffer>();

    return true;
}

void EmissivityPass::setInputs( const SharedTextures& depthBuffer,
                                const SharedTextures& ambientOcclusion ) {
    addImportedTextures( {"Emissivity::Depth", depthBuffer.second} );
    addImportedTextures( {"Emissivity::AmbOcc", ambientOcclusion.second} );
    m_passParams.addParameter( "amb_occ_sampler", ambientOcclusion.second.get() );
}

void EmissivityPass::setOutput( const SharedTextures& colorBuffer ) {
    m_outputTexture = colorBuffer;
}

bool EmissivityPass::update() {
    return true;
}

void EmissivityPass::resize( size_t width, size_t height ) {
    // Only resize the owned textures. Imported one are resized by their owner
    for ( auto& t : m_localTextures )
    {
        t.second->resize( width, height );
    }

    for ( auto& t : m_sharedTextures )
    {
        t.second->resize( width, height );
    }

    m_fbo->bind();
    m_fbo->attachTexture( GL_DEPTH_ATTACHMENT, m_importedTextures["Emissivity::Depth"]->texture() );
    m_fbo->attachTexture( GL_COLOR_ATTACHMENT0, m_outputTexture.second->texture() );
#ifdef PASSES_LOG
    if ( m_fbo->checkStatus() != GL_FRAMEBUFFER_COMPLETE )
    { LOG( logERROR ) << "FBO Error (EmissivityPass::resize): " << m_fbo->checkStatus(); }
#endif
    // finished with fbo, undbind to bind default
    globjects::Framebuffer::unbind();
}

void EmissivityPass::execute( const Ra::Engine::Data::ViewingParameters& viewParams ) const {
    using ClearColor = Ra::Core::Utils::Color;

    m_fbo->bind();
    // only draw into 1 buffers (Color)
    GL_ASSERT( glDrawBuffers( 1, buffers ) );
    GL_ASSERT( glDepthMask( GL_FALSE ) );
    GL_ASSERT( glColorMask( GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE ) );

    GL_ASSERT( glEnable( GL_DEPTH_TEST ) );
    GL_ASSERT( glDepthFunc( GL_EQUAL ) );
    GL_ASSERT( glDisable( GL_BLEND ) );

    for ( const auto& ro : *m_objectsToRender )
    {
        ro->render( m_passParams, viewParams, passIndex() );
    }
}

bool EmissivityPass::buildRenderTechnique( const Ra::Engine::Rendering::RenderObject* ro,
                                           Ra::Engine::Rendering::RenderTechnique& rt ) const {
    std::string resourcesRootDir = getResourcesDir();
    auto mat = const_cast<Ra::Engine::Rendering::RenderObject*>( ro )->getMaterial();
    // Volumes are not used in EmissivityPass
    if ( mat->getMaterialAspect() == Ra::Engine::Data::Material::MaterialAspect::MAT_DENSITY )
    { return false; }
    if ( auto cfg = Ra::Engine::Data::ShaderConfigurationFactory::getConfiguration(
             {"EmissivityPass::" + mat->getMaterialName()} ) )
    { rt.setConfiguration( *cfg, passIndex() ); }
    else
    {
        // Build the shader configuration
        Ra::Engine::Data::ShaderConfiguration theConfig{
            {"EmissivityPass::" + mat->getMaterialName()},
            resourcesRootDir + "Shaders/EmissivityPass/emissivitypass.vert.glsl",
            resourcesRootDir + "Shaders/EmissivityPass/emissivitypass.frag.glsl"};
        // add the material interface to the fragment shader
        theConfig.addInclude( "\"" + mat->getMaterialName() + ".glsl\"",
                              Ra::Engine::Data::ShaderType::ShaderType_FRAGMENT );
        // Add to the ShaderConfigManager
        Ra::Engine::Data::ShaderConfigurationFactory::addConfiguration( theConfig );
        // Add to the RenderTechniq
        rt.setConfiguration( theConfig, passIndex() );
    }
    rt.setParametersProvider( mat, passIndex() );
    return true;
}

void EmissivityPass::setBackground( const Ra::Core::Utils::Color& bgk ) {
    m_bgkColor = bgk;
}
} // namespace RadiumNBR
