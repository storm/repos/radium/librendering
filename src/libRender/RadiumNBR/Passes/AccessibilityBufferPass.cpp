#include <RadiumNBR/Passes/AccessibilityBufferPass.hpp>

#ifdef PASSES_LOG
#    include <Core/Utils/Log.hpp>
using namespace Ra::Core::Utils; // log
#endif

#include <Core/Geometry/MeshPrimitives.hpp>

#include <Engine/Data/Mesh.hpp>
#include <Engine/Data/ShaderProgramManager.hpp>
#include <Engine/Data/Texture.hpp>
#include <Engine/Data/ViewingParameters.hpp>
#include <Engine/RadiumEngine.hpp>

#include <globjects/Framebuffer.h>

namespace RadiumNBR {
using namespace gl;

static const GLenum buffer = {GL_COLOR_ATTACHMENT0};

AccessibilityBufferPass::AccessibilityBufferPass(
    const std::vector<RenderObjectPtr>* objectsToRender,
    const Ra::Core::Utils::Index& idx ) :
    RenderPass( "Accesibility buffer pass", idx, objectsToRender ) {}

AccessibilityBufferPass::~AccessibilityBufferPass() = default;

bool AccessibilityBufferPass::initializePass( size_t width,
                                              size_t height,
                                              Ra::Engine::Data::ShaderProgramManager* shaderMngr ) {
    // Initialize the fbo
    m_fbo     = std::make_unique<globjects::Framebuffer>();
    m_blurfbo = std::make_unique<globjects::Framebuffer>();

    Ra::Engine::Data::TextureParameters texparams;
    texparams.width     = width;
    texparams.height    = height;
    texparams.target    = GL_TEXTURE_2D;
    texparams.minFilter = GL_LINEAR;
    texparams.magFilter = GL_LINEAR;

    texparams.internalFormat = GL_RGBA32F;
    texparams.format         = GL_RGBA;

    texparams.type = GL_FLOAT;
    texparams.name = "SSDO::AOBuffer";
    m_sharedTextures.insert(
        {texparams.name, std::make_shared<Ra::Engine::Data::Texture>( texparams )} );

    texparams.minFilter = GL_LINEAR;
    texparams.magFilter = GL_LINEAR;
    texparams.name      = "SSDO::rawAO";
    m_rawAO             = std::make_unique<Ra::Engine::Data::Texture>( texparams );

    // The shader caller
    Ra::Core::Geometry::TriangleMesh mesh =
        Ra::Core::Geometry::makeZNormalQuad( Ra::Core::Vector2( -1.f, 1.f ) );
    auto qm = std::make_unique<Ra::Engine::Data::Mesh>( "caller" );
    qm->loadGeometry( std::move( mesh ) );
    m_quadMesh = std::move( qm );
    m_quadMesh->updateGL();

    // The shader
    std::string resourcesRootDir = getResourcesDir();
    auto added                   = shaderMngr->addShaderProgram(
        {{"SSDO"},
         resourcesRootDir + "Shaders/AccessibilityPass/ssao.vert.glsl",
         resourcesRootDir + "Shaders/AccessibilityPass/ssao.frag.glsl"} );
    if ( added ) { m_shader = added.value(); }
    else
    { return false; }

    added = shaderMngr->addShaderProgram(
        {{"blurSSDO"},
         resourcesRootDir + "Shaders/AccessibilityPass/ssao.vert.glsl",
         resourcesRootDir + "Shaders/AccessibilityPass/blurao.frag.glsl"} );
    if ( added )
    {
        m_blurShader = added.value();
        return true;
    }
    return false;
}

bool AccessibilityBufferPass::update() {
    // nothing to do
    auto aabb = Ra::Engine::RadiumEngine::getInstance()->computeSceneAabb();
    if ( aabb.isEmpty() ) { m_sceneDiag = 1_ra; }
    else
    { m_sceneDiag = aabb.diagonal().norm(); }
    return true;
}

void AccessibilityBufferPass::resize( size_t width, size_t height ) {
    for ( auto& t : m_sharedTextures )
    {
        t.second->resize( width, height );
    }
    m_rawAO->resize( width, height );

    m_fbo->bind();
    m_fbo->attachTexture( GL_COLOR_ATTACHMENT0, m_rawAO->texture() );
#ifdef PASSES_LOG
    if ( m_fbo->checkStatus() != GL_FRAMEBUFFER_COMPLETE )
    { LOG( logERROR ) << "FBO Error (GeomPrePass::resize): " << m_fbo->checkStatus(); }
#endif
    // finished with fbo, undbind to bind default

    m_blurfbo->bind();
    m_blurfbo->attachTexture( GL_COLOR_ATTACHMENT0, m_sharedTextures["SSDO::AOBuffer"]->texture() );
#ifdef PASSES_LOG
    if ( m_blurfbo->checkStatus() != GL_FRAMEBUFFER_COMPLETE )
    { LOG( logERROR ) << "FBO Error (GeomPrePass::resize): " << m_blurfbo->checkStatus(); }
#endif
    globjects::Framebuffer::unbind();
}

void AccessibilityBufferPass::execute(
    const Ra::Engine::Data::ViewingParameters& viewParams ) const {
    m_fbo->bind();
    GL_ASSERT( glDrawBuffers( 1, &buffer ) );
    GL_ASSERT( glClearBufferfv( GL_COLOR, 0, Ra::Core::Utils::Color::White().data() ) );
    GL_ASSERT( glDisable( GL_DEPTH_TEST ) );
    GL_ASSERT( glDepthMask( GL_FALSE ) );

    m_shader->bind();
    Ra::Core::Matrix4 viewproj = viewParams.projMatrix * viewParams.viewMatrix;

    m_shader->setUniform( "transform.mvp", viewproj );
    m_shader->setUniform( "transform.proj", viewParams.projMatrix );
    m_shader->setUniform( "transform.view", viewParams.viewMatrix );

    const auto tn = m_importedTextures.find( "SSDO::NormalSampler" );
    m_shader->setUniform( "normal_sampler", tn->second.get(), 0 );
    const auto tp = m_importedTextures.find( "SSDO::PositionSampler" );
    m_shader->setUniform( "position_sampler", tp->second.get(), 1 );
    m_shader->setUniform( "dir_sampler", m_sphereSampler->asTexture(), 2 );
    m_shader->setUniform( "ssdoRadius", m_aoRadius / 100_ra * m_sceneDiag );

    m_quadMesh->render( m_shader );
    GL_ASSERT( glEnable( GL_DEPTH_TEST ) );
    GL_ASSERT( glDepthMask( GL_TRUE ) );
    m_fbo->unbind();

    m_blurfbo->bind();
    m_blurShader->bind();
    GL_ASSERT( glDrawBuffers( 1, &buffer ) );
    GL_ASSERT( glClearBufferfv( GL_COLOR, 0, Ra::Core::Utils::Color::White().data() ) );
    GL_ASSERT( glDisable( GL_DEPTH_TEST ) );
    GL_ASSERT( glDepthMask( GL_FALSE ) );

    m_blurShader->setUniform( "transform.mvp", viewproj );
    m_blurShader->setUniform( "transform.proj", viewParams.projMatrix );
    m_blurShader->setUniform( "transform.view", viewParams.viewMatrix );
    m_blurShader->setUniform( "ao_sampler", m_rawAO.get(), 0 );
    m_quadMesh->render( m_blurShader );
    GL_ASSERT( glEnable( GL_DEPTH_TEST ) );
    GL_ASSERT( glDepthMask( GL_TRUE ) );
    m_blurfbo->unbind();
}

/// These inputs must be computed before executing this pass
/// positions and normals must be computed in world space
void AccessibilityBufferPass::setInputs( const SharedTextures& positions,
                                         const SharedTextures& normals ) {
    m_importedTextures["SSDO::PositionSampler"] = positions.second;
    m_importedTextures["SSDO::NormalSampler"]   = normals.second;
}

/// Set the sampler to use to compute SSDO
void AccessibilityBufferPass::setSampler( std::unique_ptr<SphereSampler> sampler ) {
    m_sphereSampler = std::move( sampler );
}

// Nothing to do, this pass is screen space
bool AccessibilityBufferPass::buildRenderTechnique(
    const Ra::Engine::Rendering::RenderObject* ro,
    Ra::Engine::Rendering::RenderTechnique& rt ) const {
    return true;
};
} // namespace RadiumNBR
