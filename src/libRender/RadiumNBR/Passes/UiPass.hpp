#pragma once
#include <RadiumNBR/RenderPass.hpp>

#include <Core/Utils/Color.hpp>

namespace Ra::Engine::Data {
class ShaderProgram;
} // namespace Ra::Engine::Data

namespace globjects {
class Framebuffer;
}

namespace RadiumNBR {
/// Render pass that draws the objects as UI elements
class UIPass : public RenderPass
{
  public:
    UIPass( const std::vector<RenderObjectPtr>* objectsToRender);
    ~UIPass() override;

    bool buildRenderTechnique( const Ra::Engine::Rendering::RenderObject* ro,
                               Ra::Engine::Rendering::RenderTechnique& rt ) const override;
    bool initializePass( size_t width,
                         size_t height,
                         Ra::Engine::Data::ShaderProgramManager* shaderMngr ) override;
    bool update() override;
    void resize( size_t width, size_t height ) override;
    void execute( const Ra::Engine::Data::ViewingParameters& viewParams ) const override;

    /// Add the output colorBuffer
    void setOutput( const Ra::Engine::Data::Texture* colorBuffer );

    /// These inputs must be computed before executing this pass
    /// depth buffer ? // TODO verify we need this
    void setInputs( const SharedTextures& depthBuffer );

  private:
    /// The framebuffer used to render this pass
    std::unique_ptr<globjects::Framebuffer> m_fbo{nullptr};

    /// The Shader manager to use when building shaders
    Ra::Engine::Data::ShaderProgramManager* m_shaderMngr{nullptr};

    /// The color texture for output.Stored here for easy access.
    const Ra::Engine::Data::Texture* m_outputTexture;
};
} // namespace RadiumNBR
