#pragma once
#include <RadiumNBR/EnvMap.hpp>
#include <RadiumNBR/RenderPass.hpp>

namespace Ra::Engine::Data {
class ShaderProgram;
} // namespace Ra::Engine::Data

namespace globjects {
class Framebuffer;
}

namespace RadiumNBR {
/// Render pass that computes the lighting from an environment map or an "ambiant" color
/// If there is no envmap, only the ambiant is computed as 0.01 * diffuseColor
/// For now, this pass handle only one envmap.
/// @note This pass must be executed before any other passes that might blend their result with the
/// output color buffer.
/// @note This pass does not draw the skybox defined by the envmap. This should be done before any
/// pass, when clearing the screen (@see RadiumNBR::ClearPass ans an example)
class EnvLightPass : public RenderPass
{
  public:
    EnvLightPass( const std::vector<RenderObjectPtr>* objectsToRender,
                  const Ra::Core::Utils::Index& idx );
    ~EnvLightPass() override;

    bool buildRenderTechnique( const Ra::Engine::Rendering::RenderObject* ro,
                               Ra::Engine::Rendering::RenderTechnique& rt ) const override;
    bool initializePass( size_t width,
                         size_t height,
                         Ra::Engine::Data::ShaderProgramManager* shaderMngr ) override;
    bool update() override;
    void resize( size_t width, size_t height ) override;
    void execute( const Ra::Engine::Data::ViewingParameters& viewParams ) const override;

    /// These inputs must be computed before executing this pass
    /// depth buffer and AmbientOcclusion
    void setInputs( const SharedTextures& depthBuffer, const SharedTextures& ambientOcclusion );

    /// set the output texture to use for the color
    void setOutput( const SharedTextures& colorBuffer );

    /// set the env map to use
    void setEnvMap( std::shared_ptr<EnvMap> envmp );

    /// set the strength (aka "power") of the env map
    void setEnvStrength( int s ) {
        if ( m_envmap )
        {
            m_envStrength = float( s ) / 100.f;
            m_envmap->setEnvStrength( m_envStrength );
        }
    }

    /// get the strength (aka "power") of the env map
    int getEnvStrength() const { return int( m_envStrength * 100 ); }

  private:
    /// The framebuffer used to render this pass
    std::unique_ptr<globjects::Framebuffer> m_fbo{nullptr};

    /// The Shader manager to use when building shaders
    Ra::Engine::Data::ShaderProgramManager* m_shaderMngr{nullptr};

    /// The color texture for output.Stored here for easy access.
    SharedTextures m_outputTexture;

    /// The Environment to used for envmap lighting
    std::shared_ptr<EnvMap> m_envmap{nullptr};

    /// The strength of the envmap
    float m_envStrength;
};
} // namespace RadiumNBR
