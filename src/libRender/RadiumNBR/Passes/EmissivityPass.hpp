#pragma once
#include <RadiumNBR/RenderPass.hpp>

#include <Core/Utils/Color.hpp>

namespace Ra::Engine::Data {
class ShaderProgram;
} // namespace Ra::Engine::Data

namespace globjects {
class Framebuffer;
}

namespace RadiumNBR {
/// Render pass that blends the emissivity of the scene with its output
/// @todo extend this class so that it implement "glow" effect on emissivity
class EmissivityPass : public RenderPass
{
  public:
    EmissivityPass( const std::vector<RenderObjectPtr>* objectsToRender,
                    const Ra::Core::Utils::Index& idx );
    ~EmissivityPass() override;

    bool buildRenderTechnique( const Ra::Engine::Rendering::RenderObject* ro,
                               Ra::Engine::Rendering::RenderTechnique& rt ) const override;
    bool initializePass( size_t width,
                         size_t height,
                         Ra::Engine::Data::ShaderProgramManager* shaderMngr ) override;
    bool update() override;
    void resize( size_t width, size_t height ) override;
    void execute( const Ra::Engine::Data::ViewingParameters& viewParams ) const override;

    /// Add the output colorBuffer
    void setOutput( const SharedTextures& colorBuffer );

    /// These inputs must be computed before executing this pass
    /// depth buffer and AmbientOcclusion
    void setInputs( const SharedTextures& depthBuffer, const SharedTextures& ambientOcclusion );

    /// Set the color background for the picture.
    void setBackground( const Ra::Core::Utils::Color& bgk );

  private:
    /// The framebuffer used to render this pass
    std::unique_ptr<globjects::Framebuffer> m_fbo{nullptr};

    /// The Shader manager to use when building shaders
    Ra::Engine::Data::ShaderProgramManager* m_shaderMngr{nullptr};

    /// The color texture for output.Stored here for easy access.
    SharedTextures m_outputTexture;

    /// Background Color
    Ra::Core::Utils::Color m_bgkColor;
};
} // namespace RadiumNBR
