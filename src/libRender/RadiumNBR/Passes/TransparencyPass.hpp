#pragma once
#include <Engine/Data/DisplayableObject.hpp>
#include <RadiumNBR/RenderPass.hpp>

#include <Core/Utils/Color.hpp>

namespace Ra::Engine {
namespace Data {
class ShaderProgram;
}
namespace Scene {
class LightManager;
}
} // namespace Ra::Engine

namespace globjects {
class Framebuffer;
}

namespace RadiumNBR {
/// Render pass that computes the lighting from local lights
class TransparencyPass : public RenderPass
{
  public:
    TransparencyPass( const std::vector<RenderObjectPtr>* objectsToRender,
                      const Ra::Core::Utils::Index& idx );
    ~TransparencyPass() override;

    bool buildRenderTechnique( const Ra::Engine::Rendering::RenderObject* ro,
                               Ra::Engine::Rendering::RenderTechnique& rt ) const override;
    bool initializePass( size_t width,
                         size_t height,
                         Ra::Engine::Data::ShaderProgramManager* shaderMngr ) override;
    bool update() override;
    void resize( size_t width, size_t height ) override;
    void execute( const Ra::Engine::Data::ViewingParameters& viewParams ) const override;

    /// These inputs must be computed before executing this pass
    /// depth buffer and AmbientOcclusion
    void setInputs( const SharedTextures& depthBuffer, const SharedTextures& ambientOcclusion );

    /// set the output texture to use for the color
    void setOutput( const SharedTextures& colorBuffer );

    /// Set the lightManager
    void setLightManager( const Ra::Engine::Scene::LightManager* lm );

  private:
    /// The framebuffer used to render this pass
    std::unique_ptr<globjects::Framebuffer> m_fbo{nullptr};

    /// The framebuffer to render the Transparent objects
    std::unique_ptr<globjects::Framebuffer> m_oitFbo{nullptr};

    /// The quad to be drawn for shader invocation
    std::unique_ptr<Ra::Engine::Data::Displayable> m_quadMesh{nullptr};

    /// The shader that compose the volume onto the color picture (the Radium shader manager has
    /// ownership)
    const Ra::Engine::Data::ShaderProgram* m_shader{nullptr};

    /// The Shader manager to use when building shaders
    Ra::Engine::Data::ShaderProgramManager* m_shaderMngr{nullptr};

    /// The color texture for output.Stored here for easy access.
    SharedTextures m_outputTexture;

    /// The light manager to use
    const Ra::Engine::Scene::LightManager* m_lightmanager;
};
} // namespace RadiumNBR
