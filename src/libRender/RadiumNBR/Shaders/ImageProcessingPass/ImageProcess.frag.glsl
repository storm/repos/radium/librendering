layout( location = 0 ) out vec4 out_color;

uniform sampler2D depth_sampler;
uniform sampler2D image_sampler;
in vec2 varTexcoord;

vec2 imgSize          = vec2( textureSize( image_sampler, 0 ) );
const int halfsize    = 40;
const float dirFactor = 1.;
const float randCeil  = 0.3;

#if 1
float rand( vec2 co ) {
    return fract( sin( dot( co.xy, vec2( 12.9898, 78.233 ) ) ) * 43758.5453 );
}
float whiteNoise( in vec2 at ) {
    return ( rand( floor( at * randCeil ) / imgSize ) );
}
#else
// noise (hash) functions : taken from https://www.shadertoy.com/view/4djSRW
float hash12(vec2 p)
{
    vec3 p3  = fract(vec3(p.xyx) * .1031);
    p3 += dot(p3, p3.yzx + 33.33);
    return fract((p3.x + p3.y) * p3.z);
}

#define ITERATIONS 1
float whiteNoise( in vec2 at ) {
    float a = 0.0;
    for (int t = 0; t < ITERATIONS; t++) {
        float v = float(t+1)*.152;
        vec2 pos = (at * v + 1517.01);
        a += hash12(pos);
    }
    return a/float(ITERATIONS);
}
#undef ITERATIONS
#endif

// desc : texture that contains direction to convolve
// invertdir invert (i.e. perpendicular) direction
vec4 lic( in sampler2D desc, in bool invertdir ) {

    vec2 dir = texelFetch( desc, ivec2( gl_FragCoord.xy ), 0 ).xy;
    if (length(dir) == 0) {return vec4 (0);}

    if ( invertdir ) dir = vec2( dir.y, -dir.x );

    vec4 res = vec4( whiteNoise( gl_FragCoord.xy ) );
    vec2 currentdir = dir;
    vec2 coord      = gl_FragCoord.xy;

    for ( int i = 0; i < halfsize; i++ )
    {
        coord = coord + dirFactor * currentdir;
        res += vec4( whiteNoise( coord ) );
        currentdir = texelFetch( desc, ivec2( coord ), 0 ).xy;
        if ( invertdir ) currentdir = vec2( currentdir.y, -currentdir.x );
    }

    coord      = gl_FragCoord.xy;
    currentdir = dir;
    for ( int i = 0; i < halfsize; i++ )
    {
        coord = coord - dirFactor * currentdir;
        res += vec4( whiteNoise( coord ) );
        currentdir = texelFetch( desc, ivec2( coord ), 0 ).xy;
        if ( invertdir ) currentdir = vec2( currentdir.y, -currentdir.x );
    }

    res = res / ( 2.0f * float( halfsize ) + 1.0f );
    return smoothstep( 0.1, 0.9, res );
}

void main() {
    /*
    // 4x4 filtering for test
    const int half_width = 2;
    vec2 texelSize = 1.0 / vec2(textureSize(image_sampler, 0));
    vec3 result = vec3(0.0);
    for (int x = -half_width; x < half_width; ++x)
    {
        for (int y = -half_width; y < half_width; ++y)
        {
            vec2 offset = vec2(float(x), float(y)) * texelSize;
            result += texture(image_sampler, varTexcoord + offset).rgb;
        }
    }
    out_color = vec4(result / (4 * half_width * half_width), 1);
    */

    out_color = lic( image_sampler, true );
}
