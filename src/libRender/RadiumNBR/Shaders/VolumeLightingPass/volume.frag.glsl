// Fragment position in model space
layout (location = 0) in vec3 in_position;
// view position in model space
layout (location = 1) in vec3 in_eyeInModelSpace;

// The modeltoimage matrix
flat in mat4 invbiasmvp;
flat in mat4 world2model;

// the depth and color texture whre the volume is rendered.
uniform sampler2D imageColor;
uniform sampler2D imageDepth;

// The resulting color
out vec4 out_color;

#include "DefaultLight.glsl"

/* ---------------------------------------------------------------------------------------- */
/*              This might be #included from a "material.glsl" file                         */
/* ---------------------------------------------------------------------------------------- */
// Subsampling of shadow rays
#define SHADOW_RAY_SUBSAMPLING 4
// Compute attenuated incident lighting
#define ATTENUATED_LIGHTING
// Compute single scattering
#define SINGLE_SCATTERING

/* ---------------------------------------------------------------------------------------- */
// https://www.alanzucconi.com/2017/10/10/atmospheric-scattering-1/
// https://www.pbrt.org/fileformat-v3.html#media-world
// This material definition follows the pbrt v3 heterogeneous participating media definition
struct Material
{
// The density grid
    sampler3D density;
// Absorption coefficient, default to (0.0011, 0.0024, 0.014, 1)
    vec4 sigma_a;
// Scattering coefficient, default to (2.55, 3.21, 3.77, 1)
    vec4 sigma_s;
// phase function assymetry factor, default to 0
    float g;
// Scale factor applied to the absorption and scattering coefficients
    float scale;
// step size for ray marching
    float stepsize;
// Transformlation matrix to the caconical [0, 1]^3 domain
    mat4 modelToDensity;
};

// Some usefull constant
float Inv4Pi = 0.07957747154594766788;

// Generate pseudo rando; number using VdC sequence
float VanderCorput(uint bits) {
    bits = (bits << 16u) | (bits >> 16u);
    bits = ((bits & 0x55555555u) << 1u) | ((bits & 0xAAAAAAAAu) >> 1u);
    bits = ((bits & 0x33333333u) << 2u) | ((bits & 0xCCCCCCCCu) >> 2u);
    bits = ((bits & 0x0F0F0F0Fu) << 4u) | ((bits & 0xF0F0F0F0u) >> 4u);
    bits = ((bits & 0x00FF00FFu) << 8u) | ((bits & 0xFF00FF00u) >> 8u);
    return float(bits) * 2.3283064365386963e-10; // / 0x100000000
}

// The Henyey-Greenstein phase function
float phaseHG(float cosTheta, float g) {
    float denom = 1 + g * g + 2 * g * cosTheta;
    return Inv4Pi * (1 - g * g) / (denom * sqrt(denom));
}

/* ---------------------------------------------------------------------------------------- */
/* Get the attenuation at a point in the volume, in the given direction, until the ray goes */
/* out of the volume                                                                        */
/* When called, the tr para;eter must be initialized to 1 or to the base transmission       */
/* ---------------------------------------------------------------------------------------- */

bool getTr(Material volume, vec3 p, vec3 dir, inout vec3 tr) {
    // sigma_t for the volume, that will be modulated by the density.
    // Here, we precompute the quantum for the integration
    vec3 sigma_t = (volume.sigma_a.rgb + volume.sigma_s.rgb) * SHADOW_RAY_SUBSAMPLING*volume.stepsize;
    bool hit = false;
    p += dir * VanderCorput( uint(dot(p, p)) ) * volume.stepsize;
    for (;;) {
        float density = texture(volume.density, p).r;
        if (density > 0) {
            hit = true;
            tr *= exp(- density * sigma_t);
        }
        // go to next point
        p += dir * SHADOW_RAY_SUBSAMPLING*volume.stepsize;
        // volume test
        if (any(greaterThan(p, vec3(1.0)))
        || any(lessThan(p, vec3(0.0)))
        || all(lessThan(tr, vec3(0.00001))))
        break;
    }
    return hit;
}

/* The following two functions mut be adatped to the lighting process */
// compute the attenation of the light (miss the light intensity parameter ...
vec3 lightColor(Material volume, vec3 p, Light l) {
    //vec3 lightColor = lightContributionFrom(l, p);
    vec3 lightColor = l.color.xyz;
    vec3 Tr = vec3(1);
    #ifdef ATTENUATED_LIGHTING
    vec3 dirLight = getLightDirection(l, p);
    getTr(volume, p, dirLight, Tr);
    #endif
    return Tr*lightColor;
}

#ifdef SINGLE_SCATTERING
// compute single scattering at p
vec3 inscatter(Material volume, vec3 p, vec3 dir, Light l) {
    vec3 dirLight = getLightDirection(l, p);
    return Inv4Pi*volume.sigma_s.rgb * phaseHG(dot(dir, dirLight), volume.g) * lightColor(volume, p, l);
}
#else
// Compute multiple scattering in presence of a transfer matrix, single scattering else.
vec3 inscatter(Material volume, vec3 p, vec3 dir, Light l) {
    vec3 dirLight = getLightDirection(l, p);
    return Inv4Pi*volume.sigma_s.rgb * phaseHG(dot(dir, dirLight), volume.g) * lightColor(volume, p, l);
}
    #endif

/* ---------------------------------------------------------------------------------------- */
/*
    Volumetric does not fullfill GLSL material interface.
    When compositing in other shader system, specific tasks must be realized
*/
/* ---------------------------------------------------------------------------------------- */

// march along the ray.
// Return, in color, the color acculumated into the volume and, in tr, the final transmittance
// p is the starting point on the boundary of the volume
// dir is the (normalized) direction of the ray
// light is the light source
// This functions assume that p, dir and light are expressed in the unit cube frame [0,1]^3, that is the local frame
// of the volume
bool raymarch(Material volume, vec3 p, vec3 dir, Light l, inout float tmax, inout vec3 color, inout vec3 tr) {
    // sigma_t for the volume, that will be modulated by the density. Here, we precompute the quantum for the integration
    vec3 sigma_t = (volume.sigma_a.rgb + volume.sigma_s.rgb) * volume.stepsize;
    // The ray marching loop
    bool hit = false;
    float t = VanderCorput( uint(dot(p, p)) ) * volume.stepsize;
    p += dir * t;
    for (;;) {
        float density = texture(volume.density, p).r * volume.scale;
        if (density > 0) {
            hit = true;
            tr *= exp(- density * sigma_t);
            color += tr * density * inscatter(volume, p, dir, l) * volume.stepsize;
        }
        p += dir * volume.stepsize;
        t += volume.stepsize;
        if ( t > tmax ||
                any(greaterThan(p, vec3(1.0))) ||
                any(lessThan(p, vec3(0.0))) ||
                all(lessThan(tr, vec3(0.00001))) ) {
            tmax = t;
            break;
        }
    }
    return hit;
}

uniform Material material;

/* ---------------------------------------------------------------------------------------- */

void main() {
    // get image informations
    ivec3 texSize = textureSize(material.density, 0);
    ivec2 colorImageSize = textureSize(imageColor, 0);
    // Transform all geometries from model-space to the canonical [0,1]^3 grid space
    vec4 pos = material.modelToDensity * vec4(in_position, 1);
    vec4 eye = material.modelToDensity * vec4(in_eyeInModelSpace, 1);
    pos.xyz /= texSize;
    eye.xyz /= texSize;
    // compute the ray dir in canonical space
    vec3 dir = pos.xyz - eye.xyz;
    float dt = length(dir);
    dir = normalize(dir);
    // Compute the depth limit to an object inside the volume
    vec3 fpos = gl_FragCoord.xyz / vec3(colorImageSize, 1);
    vec3 objectDepth = vec3(fpos.xy, texture(imageDepth, fpos.xy).r);
    if (objectDepth.z < 1) {
        // Transform limit point to model space
        vec4 objPoint = invbiasmvp * vec4(objectDepth, 1);
        objPoint/=objPoint.w;
        // Transform limit point to canonical space
        objPoint = material.modelToDensity * vec4(objPoint.xyz, 1);
        objPoint.xyz /= texSize;
        vec3 dirObject = objPoint.xyz - eye.xyz;
        float tmax = length(dirObject);
        // compute the depth limit
        dt = (tmax-dt);
    } else {
        // no limit, set it to infinity
        dt = 10;
    }
    // compute the light information in canonical space
    Light l = transformLight(light, material.modelToDensity * world2model );
    switch (light.type) {
        case 0: break;
        case 1: l.point.position/= texSize;
        break;
        case 2: l.spot.position/= texSize;
        break;
        default : break;
    }
    // raymarch on the volume
    vec3 volColor=vec3(0);
    vec3 volTransmitance=vec3(1);
    bool hit = raymarch(material, pos.xyz, dir, l, dt, volColor, volTransmitance);

    if (!hit) {
        discard;// no medium found
    }
    vec4 backColor = texture(imageColor, fpos.xy);
    out_color = vec4(volColor + backColor.rgb * volTransmitance, 1);
}
