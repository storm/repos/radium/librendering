#pragma once

#include <QDialog>
#include <QDialogButtonBox>
#include <QLabel>
#include <QTabWidget>

#include <map>

namespace Mara {
/**
 * Information tab about the loaded scene.
 * @todo make this tab more instructive about the scene (geometry stats, bounding box, ...)
 */
class InformationTab : public QWidget
{
    Q_OBJECT

  public:
    /** Constructors and destructor.
     *  https://en.cppreference.com/w/cpp/language/rule_of_three
     */
    /** @{ */
    explicit InformationTab( QWidget* parent = nullptr );
    InformationTab( const InformationTab& ) = delete;
    InformationTab& operator=( const InformationTab& ) = delete;
    InformationTab( InformationTab&& )                 = delete;
    InformationTab& operator=( InformationTab&& ) = delete;
    ~InformationTab() override                    = default;
    /**@}*/

    /// Display the information about the file
    void setFileInfo( const QString& fileName,
                      const std::tuple<size_t, size_t, size_t>& sceneStats );

  private:
    QLabel* m_fileNameDisplay;
    QLabel* m_pathNameDisplay;
    QLabel* m_sizeValueDisplay;
    QLabel* m_objCount;
    QLabel* m_polyCount;
    QLabel* m_vertCount;
};

/**
 * The main control window for the Radium player
 */
class ControlDialogWindow : public QDialog
{
    Q_OBJECT

  public:
    /**
     * Constructors and destructor follow the 'rule of five'
     *  https://en.cppreference.com/w/cpp/language/rule_of_three
     */
    /** @{ */
    explicit ControlDialogWindow( QWidget* parent = nullptr );
    ControlDialogWindow( const ControlDialogWindow& ) = delete;
    ControlDialogWindow& operator=( const ControlDialogWindow& ) = delete;
    ControlDialogWindow( ControlDialogWindow&& )                 = delete;
    ControlDialogWindow& operator=( ControlDialogWindow&& ) = delete;
    ~ControlDialogWindow() override                         = default;
    /**@}*/

    /// Fill the scene information tab
    void setFileInfo( const std::string& fileName,
                      const std::tuple<size_t, size_t, size_t>& sceneStats );

    /**
     * Add a control panel to the window
     * @param tab the control panel to add
     * @param tabname its name
     */
    void addTab( QWidget* tab, const std::string& tabname );

    /**
     * switch to the named control panel.
     * @param tabname the control panel name
     */
    void showTab( const std::string& tabname );

  private:
    QTabWidget* m_tabWidget;
    QDialogButtonBox* m_buttonBox;
    InformationTab* m_tabInfo;
    std::map<std::string, int> m_tabIndices;
};
} // namespace Mara
