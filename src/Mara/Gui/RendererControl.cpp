#include <Gui/RendererControl.hpp>

#include <QRadioButton>

#include <Engine/Rendering/Renderer.hpp>

namespace Mara {
RendererControl::RendererControl( QWidget* /*parent*/ ) {
    setupUi( this );

    auto textureSelectorLayout = new QHBoxLayout();
    textureSelectorLayout->addWidget( new QLabel( "Image to display" ) );
    m_textureList = new QComboBox();
    textureSelectorLayout->addWidget( m_textureList );
    panelLayout->addLayout( textureSelectorLayout );
    panelLayout->addStretch();

    connect( rendererList,
             static_cast<void ( QComboBox::* )( int )>( &QComboBox::currentIndexChanged ),
             [=]( int i ) { this->changeRenderer( rendererList->itemText( i ).toStdString() ); } );
    connect( m_textureList,
             static_cast<void ( QComboBox::* )( int )>( &QComboBox::currentIndexChanged ),
             [=]( int i ) { this->changeTexture( m_textureList->itemText( i ).toStdString() ); } );
    connect( b_fitCamera, &QPushButton::clicked, this, &RendererControl::fitCamera );
    connect( b_resetCamera, &QPushButton::clicked, this, &RendererControl::resetCamera );
}

void RendererControl::addRenderer( const std::string& rendererName,
                                   Ra::Engine::Rendering::Renderer* const renderer,
                                   std::function<bool()> callback,
                                   RadiumNBR::Gui::RendererPanel* controlPanel ) {
    m_renderersPanels.push_back( controlPanel );
    if ( controlPanel != nullptr )
    {
        controlPanel->setParent( confPanel );
        panelLayout->insertWidget( int( m_renderersPanels.size() ), controlPanel );
    }
    m_renderersCallbacks.emplace_back( RendererInfo( renderer, std::move( callback ) ) );
    rendererList->addItem( QString::fromStdString( rendererName ) );
}

void RendererControl::changeRenderer( const std::string& renderer ) {
    if ( m_currentRenderer >= 0 )
    {
        if ( m_renderersPanels[m_currentRenderer] )
        { m_renderersPanels[m_currentRenderer]->hide(); }
    }

    m_currentRenderer = rendererList->currentIndex();
    m_textureList->clear();
    if ( m_renderersCallbacks[m_currentRenderer].second() )
    {
        auto texs = m_renderersCallbacks[m_currentRenderer].first->getAvailableTextures();
        for ( const auto& tex : texs )
        {
            m_textureList->addItem( tex.c_str() );
        }
        m_textureList->setCurrentIndex( 0 );
    }

    if ( m_renderersPanels[m_currentRenderer] ) { m_renderersPanels[m_currentRenderer]->show(); }
    emit rendererStateChanged();
}

void RendererControl::changeTexture( const std::string& texture ) {
    m_renderersCallbacks[m_currentRenderer].first->displayTexture( texture );
    emit rendererStateChanged();
}
} // namespace Mara
